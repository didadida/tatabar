declare namespace wx {
    function showTip(msg: string,duration?:number): void;
    function setTitle(title: string): void;
    function modal(msg: string): Promise<any>;
    function rerender(arg: TPage<any>): void;
    function loading(msg?: string): void;
    function getImageInfo2(imgPath: string): Promise<ImageInfo>;
    function getFileSize(imgPath: string): Promise<number>;
    function createCanvasContext(canvasId: string): CanvasContext
    function compress(canvasid: string, imgPath: string, width: number, height: number): Promise<string>;
    function uploadImage(imgPath: string): Promise<ImageInfo>;
    function FileSystemManager(): void;
    function uploadFile2({ filePath: string, type: string, canvasId: string}): Promise<any>;
    function onMessage(msg:Msg):void;
    export let im:Webim
    interface ImageInfo {
        width: number//	图片原始宽度，单位px。不考虑旋转。
        height: number//	图片原始高度，单位px。不考虑旋转。
        path?: string//	图片的本地路径
        orientation?: string//	拍照时设备方向	1.9.90
        type?: string//	图片格式
        size?:number
    }
    interface CanvasToTempFilePathOptions{
        x:number
        y:number
        width:number
        height:number
    }
    interface CanvasContext {
        draw(reserve?: boolean,callback:any): void;
    }
    interface NavigateToOptions{
        events?:any
    }
}

type TPage<T> = {
    total?: number//总页数
    page?: number//页码
    limit?: number//一页显示多少条
    rows: Array<T>
}
interface Gift {
    name: string
    count: number
    url: string
}
type Gifts = Gift[]

type PartialWithRequired<T, R> = Partial<T> & R

//酒吧商品
interface BarProduct {
    Id: string//推荐商品ID
    name: string//商品名称
    price: string//价格            
    imageUrl: string//商品图片
}
//酒吧详情
type BarInfo = PartialWithRequired<{
    name: string//名称
    phone: number//电话
    address: string//地址
    lat: number//纬度
    lng: number//经度
}, {
    goodsList: BarProduct[]//推荐商品数组
}>
