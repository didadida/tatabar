interface MsgItem {
    lastMsg: string
    time: string
    identifier: string
    identifierNick: string
    unread: number
    sex: number
    thumbnail: string
    isMe: number //0收到消息 1发出消息
}

interface imUtil {
    createMsg(me, msg): MsgItem
    filterTime(seconds): string
    getProfile(im, ok): Promise<{ headUrl: any, nickName: any, sex: any }[]>
}

interface chatInfo {
    loading: boolean,
    failed: boolean,
    timeStamp: number,
    isText: boolean,
    isMe: boolean,
    content: string | {
        giftName: string
        giftCount: string
        giftUrl: string
    }
}
