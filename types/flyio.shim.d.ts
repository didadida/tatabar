import {Fly, FlyError} from 'flyio'
declare module 'flyio'{
    export interface FlyRequestConfig {
        retryed?:number
        abort:boolean
    }
    export interface FlyResponseInterceptor<V> {
        use(onSucceed?: (response: V) => any, onError?: (err: FlyError) => any): void;
    }
}
