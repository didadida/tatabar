type BarUsers = TPage<Partial<{
    userId: number //用户ID
    nickname: string //昵称
    imageUrl: string //用户头像地址
    sex: number //性别 1男2女
    sexDesc: string //性别 男 / 女
    tableName: string //桌台号，桌台名称
    isOnline: number//在线状态 0不在线，1在线
    onlineStatus: string//在线状态文字描述
}>>

type BannerList = {
    imageId: number//banner图Id
    clubUniq: number//酒吧Id
    imageUrl: string//轮播图地址
    clickUrl: string//轮播图跳转地址
}[]

type UserDetail = PartialWithRequired<{
    isSelf: number//1.是自己 2非自己
    foStatus: number//关注状态 0已关注1未关注2互相关注
    userId: number//详情用户ID
    identifier: string//详情用户的IM唯一标识 identifier
    usersig: string//详情用户的IM登录凭证 usersig
    sex: number//详情用户的性别：1男, 2女
    nickName: string//详情用户的昵称
    fansDto: number//详情用户的粉丝数量
    starDto: number//详情用户的关注数量
    thumbnail: string//详情用户头像缩略图
    userImage: string//详情用户头像原图
    hobby: string//详情用户爱好
    customSign: string//详情用户个性签名
}, {
    combos: Partial<{
        packName: string // 礼物名称
        numbers: string // 礼物数量
        imageUrlDto: string // 礼物图片
        needCoinNumDto: string // 金币数
    }>[]
    albums: HeadUrl[]
}>

type MineDetal = any

type GiftRank = Partial<{
    nickName: string//当前用户昵称
    userImage: string//当前用户头像
    sex: string|number//当前用户性别：1男, 2女
    coins: string|number//当前用户金币数
    rank: string|number//排名
    rankDesc: string//排名描述，若未上榜则显示您暂未上榜，继续努力，若已上榜则显示第几名
}> & {
    topList: {//: 排行榜数据数组 
        sex: string// 性别1男2女
        nickname: string// 昵称
        rank: string//   名次
        coins: string//   金币数}
    }[]
}

type OrderList = TPage<{
    ordersId: number//订单id
    orderNo: string//订单号
    statusType: number//状态 0待支付, 1已支付, 2已取消
    createDate: string//时间
    productName: string//商品名称
    purchaseNum: number//数量
    coinUsed: number//总金币数
    price: number//实付金额
    imageUrl: string//商品图片
    statusTypeString?:string //状态 -中文
}>

type DrinkOrder = TPage<{
    ordersId: string,//订单id
    orderNo: string,//订单号
    statusType: number,// 状态 0待支付, 1已支付, 待商家确认, 2已取消, 3已配送, 6已完成, 7已退款
    statusTypeString: string// 状态 0待支付, 1已支付, 待商家确认, 2已取消, 3已配送, 6已完成, 7已退款
    createDate: string//时间
    goodsName: string//商品名称
    purchaseNum: number//数量
    payPrice: string//实付金额
    imageUrl: string//商品图片
}>
type User = Partial<{
    userId: number// 用户ID
    userImage: string// 用户头像
    nickName: string// 昵称
    sex: number// 性别 1.男 2.女
    status: number//关注状态0关注1已取消关注2互相关注
    customSign: string //个性签名
}>
type FansAndFollow = TPage<User>


type UserInfo = Partial<{
    clubUniq: number,
    userToken: string,
    nickName: string,
    userImage: string,
    sex: string | number,
    usersig: string,
    birthday: string,
    thumbnail: string,
    userImage: string
    hobby: string,
    customSign: string
}>

interface PayOrder {
    paySuccess: number,//  1 为无需支付
    timestamp: number,//  支付时间戳
    nonceStr: number,// 支付随机数
    package: number,// 支付参数
    signType: number,// 支付参数
    paySign: number,// 支付参数
    orderNo: number,// 订单编号
    productName: string,// 产品名称
    money: number,// 支付金额
}
interface coinTrans {
    amount: number
    successfulWithdrawalsDto: number
    presentDto: number
    isBindWithdraw: number
}

type CoinDetail = TPage<Partial<{
    coinTransId: string// 金币明细id
    type: string// 类型#1.支出2.收入
    createDate: string //时间
    productName: string// 名称
    purchaseNum: string// 数量
    coinUsed: string// 变动金币数
    imageUrl: string// 商品图片
    sourceType: string// 来源类型1.购买礼物2.充值3.提现4.收礼物
    sourceId: string// 来源人id
    sourceInfo: string// 来源人(当来源类型为4收礼物时用到)
}>>
