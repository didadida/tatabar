interface ListenersResponse {
    /** 连接状态标识，OK标识连接成功FAIL - 标识连接失败*/
    ActionStatus: 'ok' | 'fail'
    /** 连接状态码，具体请参考webim.CONNECTION_STATUS常量对象*/
    ErrorCode: Webim.CONNECTION_STATUS
    /** 错误提示信息 */
    ErrorInfo: string,
    ErrorDisplay: string
}
type IMFunction<T, P> = (options: T, cbOk: (ok: ListenersResponse & P) => void, cbErr?: (err?: any) => void) => void;
class Session {
    type; id; name; icon; time; seq;
    /**type 会话类型，参考webim. SESSION_TYPE常量对象
    id 对方id , 群聊时，为群id；私聊时， 对方帐号
    name 对方名称暂未使用，选填
    icon 对方头像url，暂未使用，选填
    time 当前会话中的最新消息的时间戳，暂未使用，选填
    seq 当前会话的最新消息的序列号 暂未使用*/
    constructor(type: Webim.SESSION_TYPE, id: any, name?, icon?, time?, seq?)
    /** 获取会话类型 无 String */
    type(): string
    /** 获取对方id 无 String*/
    id()
    /** 获取对方名字，暂未使用 无 String */
    name()
    /** 获取对方头像，暂未使用 无 String */
    icon()
    /** 获取当前会话中最新消息的 时间 无 Integer */
    time()
    /** 获取当前会话中最新消息的 序列号 无 Integer */
    curMaxMsgSeq()
    /** 获取未读消息数 无 Integer */
    unread(): number
    /** 获取会话的消息数 无 Integer*/
    msgCount(): number
}

interface MsgStore {
    /**获取所有会话对象 无[webim.Session] */
    sessMap(): { [prop: string]: Session }
    /**获取对方id 无 Integer */
    sessCount(): number
    /**根据会话类型和会话ID取得相应会话 type会话类型 id 对方ID */
    sessByTypeId(type: string, id: string): Session
    /**根据会话类型和会话ID删除相应会话 type - String, 会话类型 id - String, 对方ID Boolean*/
    delSessByTypeId(type, id): boolean

}

class CText {
    text: string
    constructor(text: string)
    getText(): string
    toHtml(): string
}
class CFace {
    index: number
    data: string
    constructor(index, data)
    /**获取表情索引 */
    getIndex(): number
    /** 获取表情数据 */
    getData(): string
    /** 获取表情元素的Html代码*/
    toHtml(): string
}
class cImage {
    constructor(type: number, size: number, width: number, height: number, url: string)
    /**获取图片大小类型 无 Integer*/
    getType(): number
    /**获取大小 */
    getSize(): number
    /** 获取宽度*/
    getWidth(): number
    /** 获取高度 */
    getHeight(): number
    /** 获取地址*/
    getUrl()
    /** 获取图片信息的Html代码*/
    toHtml()
}
class CImages {
    Image: cImage
    /**图片id String */
    imageId
    /** 图片信息数组（小图，大图，原图）[Msg.Elem.Images.Image]*/
    ImageInfoArray
    constructor(imageId)
    /**向ImageInfoArray增加一张图片 */
    addImage(image): Webim.IMAGE_TYPE
    /** 获取某类图片信息 type，图片大小类型 */
    getImage(type)
    /**获取图片id */
    getImageId(): string
    /**获取图片数组元素的Html代码，*/
    toHtml(): string

}
class CLocation {
    constructor(longitude: number, latitude: number, desc: string)
    /**获取经度 */
    getLongitude(): number
    /** 获取纬度 */
    getLatitude(): number
    /** 获取描述 */
    getDesc(): string
    /** 获取位置元素的Html代码*/
    toHtml(): string
}
/** 消息元素对象(语音) */
class CSound {
    constructor(uuid: string, second: number, size: number, senderId: string, downUrl: string)
    /**获取语音id*/
    getUUID(): string
    /**获取时长 */
    getSecond(): number
    /**获取大小 */
    getSize(): number
    /**获取发送者账号*/
    getSenderId(): string
    /**获取地址 无 String*/
    getDownUrl(): string
    /**获取语音元素的Html代码*/
    toHtml(): string
}

class CFile {
    constructor(uuid: string, name: string, size: number, senderId: string, downUrl: string)
    /**获取语音id*/
    getUUID(): string
    /**获取名称 */
    getName(): number
    /**获取大小 */
    getSize(): number
    /**获取发送者账号*/
    getSenderId(): string
    /**获取地址*/
    getDownUrl(): string
    /**获取语音元素的Html代码*/
    toHtml(): string
}

class CCustom {
    content:any
    /**data 商品名 desc 商品地址 ext商品数量 */
    constructor(data: string, desc: string, ext: string)
    /**获取数据*/
    getData(): string
    /**获取描述*/
    getDesc(): string
    /**获取扩展字段*/
    getExt(): string
    /**获取文本元素的Html代码*/
    toHtml(): string
}

class GroupInfo {
    constructor(type: Webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE, value: string)
    /**获取群资料变更类型*/
    getType(): number
    /** 获取新的值，如果为空，则表示该类型的群资料没有变更*/
    getValue(): string
}
class MemberInfo {
    /** shutupTime群成员被禁言时间，0表示取消禁言 ，大于0表示被禁言时长，单位：秒 */
    constructor(userId: string, shutupTime: number)
    /**获取群成员id */
    getUserId(): string
    /** 获取群成员被禁言时间，0 表示取消禁言，大于0表示 被禁言时长，单位：秒*/
    getShutupTime(): number
}
class CGroupTip {
    groupInfoList: GroupInfo[]
    memberInfoList: MemberInfo[]
    constructor(opType: number, opUserId: string, groupId: string, groupName: string, userIdList: string[])
    /**向groupInfoList添加一条 群资料变更信息groupInfo: Msg.Elem.Gro upTip.GroupInfo*/
    addGroupInfo(groupInfo): GroupInfo
    /** 向memberInfoList添加一 条群成员变更信息memberInfo: Msg.Elem.G roupTip.MemberInfo*/
    addMemberInfo(memberInfo): MemberInfo
    /**获取操作类型，详细定义请 参考webim.GROUP_TIP_TYPE*/
    getOpType(): Webim.GROUP_TIP_TYPE
    /** 获取操作者id*/
    getOpUserId(): string
    /** 获取群id */
    getGroupId(): string
    /** 获取群名称 */
    getGroupName(): string
    /** 获取被操作的用户id列表*/
    getUserIdList(): string[]
    /** 获取新的群信息列表，群资 料变更时才有值 */
    getGroupInfoList(): GroupInfo[]
    /** 获取新的群成员信息列表， 群成员资料变更时才有值*/
    getMemberInfoList(): mberInfo[]
    /** 获取群提示消息元素的Html*/
    toHtml(): string
}
class CElem {
    /** 元素类型，具体请参考webim.MSG_ ELEMENT_TYPE */
    type;
    /** 元素内容对象 */
    static Text:typeof CText
    static Custom: typeof CCustom
    content: CText | CCustom | CFace | CFile | CGroupTip | CImages | CLocation | CSound
    constructor(type, content)
    /**获取元素类型，返回webim.MSG_ELEMENT_TYPE*/
    getType(): string
    /** 获取元素内容对象 Object，类型参考属性说 明 */
    getContent(): CText | CCustom | CFace | CFile | CGroupTip | CImages | CLocation | CSound
    /** 获取元素的Html代码，仅供参考，业务方可自定义实现*/
    toHtml(): string
}
class Msg {
    static Elem: typeof CElem
    Elems:CElem[]
    /**isSend 消息是否为自己发送标志: true：表示是我发出消息, false：表示是发给我的消息
     * subType 消息子类型: c2c消息时，webim.C2C_MSG_SUB_TYPE 群消息时，webim.GROUP_MSG_SUB_TYPE
     * fromAccount 消息发送者帐号
     * fromAccountNick 消息发送者昵称，用户没有设置昵称 时，则为发送者帐号
     * seq 消息序列号，用于消息判重
     * random 消息随机数，用于消息判重
     * time 消息时间戳，unix timestamp格
     * elems 描述消息内容的元素数组
     */
    constructor(sess: Session, isSend: boolean, seq: number, random: number, time: number, fromAccount: string, subType: number, fromAccountNick: string)
    /**获取消息所属的会话 无 webim.Session */
    getSession(): Session
    /** 获取消息是否为自己发送标 志 无 Boolean */
    getIsSend(): boolean
    /** 获取消息子类型 无 Integer */
    getSubType(): number
    /** 获取消息发送者帐号 无 String */
    getFromAccount(): string
    /** 获取消息发送者昵称，用户 没有设置昵称时，则为发送 者帐号 无 String */
    getFromAccountNick(): string
    /** 获取消息序列号 无 Integer */
    getSeq(): number
    /** 获取消息随机数 无 Integer */
    getRandom(): number
    /** 获取消息时间戳，unix timestamp格式 无 Integer */
    getTime(): number
    /** 获取描述消息内容的元素数 组 无 [webim.Msg.Elem] */
    getElems(): CElem[]
    /** 将elems转成可展示的html无 Strin*/
    toHtml(): string
    /**向elems中添加一个Text元 素 text: Msg.Elem.Text 无 */
    addText(text): void
    /** 向elems中添加一个Face元 素 face: Msg.Elem.Face 无 */
    addFace(face): void
    /** 向elems中添加一个Image s元素 image: Msg.Elem.Images 无 */
    addImage(image): void
    /** 向elems中添加一个Sound 元素 sound: Msg.Elem.Sound 无*/
    addSound(sound): void
    /** 向elems中添加一个File元 素 file: Msg.Elem.File 无 */
    addFile(file): void
    /** 向elems中添加一个Locati on元素 location: sg.Elem.Location 无 */
    addLocation(location): void
    /** 向elems中添加一个Custo m元素 custom：Msg.Elem.Cust om 无 */
    addCustom(custom): void
    /** 向elems中添加一个Group Tip元素 groupTip: Msg.Elem.GroupTip 无 */
    addGroupTip(groupTip): void
}

interface loginInfo {
    sdkAppID
    appIDAt3rd?
    Appid
    identifier
    identifierNick
    userSig
}

interface Listeners {
    /** 用于监听用户连接状态变化的函数， 选填*/
    onConnNotify?(arg: ListenersResponse): void
    /** 用于IE9(含)以下浏览器中jsonp回调 函数, 移动端可不填，pc端必填*/
    jsonpCallback?(): void
    /** 监听新消息函数，必填 Function */
    onMsgNotify(msgList: Msg[]): void
    /** 监听 新消息(直 播聊天室)事件，直播场景下必填 Function */
    onBigGroupMsgNotify?: Partial<{
        /**申请加群请求（只有管理员会收到）  */
        "1": Function,
        /** //申请加群被同意（只有申请人能够收到）  */
        "2": Function,
        /** //申请加群被拒绝（只有申请人能够收到）  */
        "3": Function,
        /** //被管理员踢出群(只有被踢者接收到)  */
        "4": Function,
        /** //群被解散(全员接收)  */
        "5": Function,
        /** //创建群(创建者接收)  */
        "6": Function,
        /** //邀请加群(被邀请者接收)  */
        "7": Function,
        /** //主动退群(主动退出者接收)  */
        "8": Function,
        /** //设置管理员(被设置者接收)  */
        "9": Function,
        /** //取消管理员(被取消者接收)  */
        "10": Function,
        /** //群已被回收(全员接收)  */
        "11": Function,
        /**用户自定义通知(默认全员接收) */
        "255": Function
    }>
    /** 监听群资料变化事件，选填 Object */
    onGroupInfoChangeNotify?(groupInfo: {
        /**群id */
        GroupId: string
        /** 新群组图标, 为空，则表示没有变化 */
        GroupFaceUrl: string
        /** 新群名称, 为空，则表示没有变化 */
        GroupName: string
        /** 新的群主id, 为空，则表示没有变化 */
        OwnerAccount: string
        /** 新的群公告, 为空，则表示没有变化 */
        GroupNotification: string
        /** 新的群简介, 为空，则表示没有变化*/
        GroupIntroduction: string

    }): void
    /** 监听（多终端同步）群系统消息事件 ，必填 Object */
    onGroupSystemNotifys?(): void
    /** 监听好友系统通知事件，选填 Object */
    onFriendSystemNotifys?: Partial<{
        /**好友表增加*/
        "1": Function,
        /** 好友表删除  */
        "2": Function,
        /** 未决增加  */
        "3": Function,
        /** 未决删除  */
        "4": Function,
        /** 黑名单增加  */
        "5": Function,
        /**黑名单删除*/
        "6": Function
    }>
    /** 监听资料系统（自己或好友）通知事 件，选填 Object */
    onProfileSystemNotifys?: {
        /**资料修改*/
        "1": onProfileModifyNotify
    }
    /** 被其他登录实例踢下线，选填 Function */
    onKickedEventCall?(): void
    /** 监听C2C系统消息通道，选填 Object*/
    onC2cEventNotifys?: {
        /** 消息已读通知 */
        "92": Function,
    }
}

interface Webim {
    /**会话类型，取值范围：C2C私聊..GROUP群聊 */
    SESSION_TYPE: {
        C2C: string
        GROUP: string
    }
    /**C2C消息子类型，取值范围：COMMON-普通消息 */
    C2C_MSG_SUB_TYPE: {
        COMMON: string
    }
    /**群消息子类型，取值范围：COMMON-普通消息.LOVEMSG –点赞消息.TIP –提示消息.REDPACKET–红包消息(优先级最高) */
    GROUP_MSG_SUB_TYPE: {
        COMMON: any
        LOVEMSG: any
        TIP: any
        REDPACKET: any
    }
    /**群提示消息类型，取值范围：JOIN进群 QUIT退群 KICK被踢出群 SET_ADMIN-被设置成管理员 CANCEL_ADMIN-被取消管理员角色
     * MODIFY_GROUP_INFO-修改群资料 MODIFY_MEMBER_INFO 修改群成员信息*/
    GROUP_TIP_TYPE: {
        JOIN: any
        QUIT: any
        KICK: any
        SET_ADMIN: any
        CANCEL_ADMIN: any
        MODIFY_GROUP_INFO: any
        MODIFY_MEMBER_INFO: any
    }
    /**群资料变更类型，取值范围：FACE_URL 群头像发生变更 NAME 群名称发生变更 OWNER 群主发生变更 NOTIFICATION 群公告发生变更 INTRODUCTION-群简介发生变更 */
    GROUP_TIP_MODIFY_GROUP_INFO_TYPE: {
        FACE_URL: any
        NAME: any
        OWNER: any
        NOTIFICATION: any
        INTRODUCTION: any
    }
    MSG_MAX_LENGTH:{
        C2C:any
        GROUP:any
    }
    /**群系统消息类型 JOIN_GROUP_REQUEST-申请加群 JOIN_GROUP_ACCEPT -加群被同意 JOIN_GROUP_REFUSE -加群被拒绝
    * KICK 被踢出群 DESTORY -群被解散 CREATE -创建群 INVITED_JOIN_GROUP_REQUEST-邀请加群 QUIT-主动退群
    * SET_ADMIN-设置管理员 CANCEL_ADMIN-取消管理员 REVOKE -群已被回收 CUSTOM-用户自定义通知 */
    GROUP_SYSTEM_TYPE: {
        JOIN_GROUP_REQUEST: any
        JOIN_GROUP_ACCEPT: any
        JOIN_GROUP_REFUSE: any
        KICK: any
        DESTORY: any
        CREATE: any
        INVITED_JOIN_GROUP_REQUEST: any
        QUIT: any
        SET_ADMIN: any
        CANCEL_ADMIN: any
        REVOKE: any
        CUSTOM: any
    }
    /**
     * 消息元素类型,TEXT-文本消息 FACE-表情消息 IMAGE-图片消息 SOUND-语音消息 FILE-文件消息 LOCATION-位置消息 CUSTOM-自定义消息 GROUP_TIP-群提示消息
才会出现）
     */
    MSG_ELEMENT_TYPE: {
        TEXT: any
        FACE: any
        IMAGE: any
        SOUND: any
        LOCATION: any
        CUSTOM: any
        GROUP_TIP: any
    }
    /**图片大小类型.SMALL-小图.LARGE-大图.ORIGIN-原图 */
    IMAGE_TYPE: {
        SMALL: any
        LARGE: any
        ORIGIN: any
    }
    /**表情对象  */
    Emotions: { [prop: string]: any }
    /**表情标识字符串和index的Map */
    EmotionDataIndexs: { [prop: string]: any }
    /**当前浏览器信息 */
    BROWSER_INFO: {
        type: 'ie' | 'safari' | 'chrome' | 'firefox' | 'opera' | 'unknow'
        ver: string
    }
    /**连接状态 ON-连接状态正常 OFF-连接已断开 RECONNECT-连接重新建立 */
    CONNECTION_STATUS: {
        ON: any
        OFF: any
        RECONNECT: any
    }
    /**消息存储中心 */
    MsgStore: MsgStore
    /**一个会话对象.包括获取会话类型（私聊还是群聊），对方帐号，未读消息数，总消息数等 */
    Session: typeof Session
    /**一条消息对象.消息发送、接收的API中都会涉及此类型的对象*/
    Msg: typeof Msg
    /**工具对象 比如格式化时间戳函数formatTimeStamp(),获取字符串（utf-8编码）所占字节数getStrBytes()等*/
    Tool: {
        formatTimeStamp(time: number, format: string): void
        getStrBytes(str: string): number
    }
    /**连接状态 在初始化sdk时，可以传递一个布尔类型的变量来控制是否在控制台打印日志 */
    Log: {
        debug(logStr: string): void
        info(logStr: string): void
        warn(logStr: string): void
        error(logStr: string): void
    }
    /**sdk登录.登录SDK，需要传入当前用户信息，新消息通知回调函数等*/
    login(loginInfo: LoginInfo, listeners: Listeners, opts: {
        isAccessFormalEnv: boolean
        isLogOn: boolean
    }, cbOk?: (ok?) => void, cbErr?: (err?) => void)
    /**上报已读消息.设置聊天会话自动已读标识  isOn - boolean, 将selSess的自动已读消息标志改为isOn * isResetAll - boolean，是否重置所有会话的自动已读标志*/
    setAutoRead(selSess: Session, isOn: boolean, isResetAll: boolean);
    /**发消息.发送消息(私聊和群聊) */
    sendMsg: IMFunction<Msg[]>
    /**获取好友历史消息 */
    getC2CHistoryMsgs(options: {
        /**好友帐号  */
        Peer_Account: string,
        /**拉取消息条数  */
        MaxCnt: number,
        /**最近的消息时间，即从这个时间点向前拉取历史消息  */
        LastMsgTime: number,
        MsgKey: msgKey
    }, cbOk: (ok: {
        MsgList: Msg[]
        Complete: 0 | 1
        LastMsgTime: number
        MsgKey: string
        MsgCount: number
    }) => void, cbErr: (err) => void);

    /**获取群历史消息 */
    syncGroupMsgs: IMFunction<{
        GroupId: string
        ReqMsgSeq: number
        ReqMsgNumber: number
    }>
    /**查询个人资料 */
    getProfilePortrait(options: {
        To_Account: string[],
        TagList: string[]
    }, cbOk: (ok: {
        UserProfileItem: {
            To_Account: string
            ProfileItem: { Tag: string, Value: string }[]
        }[]
        Fail_Account: string[]
        Invalid_Account: string[]
        ActionStatus: string,
        ErrorCode: number,
        ErrorInfo: string,
        ErrorDisplay: string
    }) => void, cbErr: (err) => void): void;
    /**设置个人资料 */
    setProfilePortrait: IMFunction<{
        ProfileItemL: { Tag: string, Value: string }[]
    }>
    /**申请添加好友 */
    applyAddFriend: IMFunction<{
        From_Account: string
        AddFriendItem: {
            To_Account: string
            AddSource: any
            AddWording: string
        }
    }>
    /**拉取好友申请*/
    getPendency: IMFunction<{
        From_Account: string
        PendencyType: string
        StartTime: number
        MaxLimited: string,
        LastSequence: number
    }>
    /**响应好友申请 */
    responseFriend: IMFunction<{
        From_Account: string
        ResponseFriendItem: {
            To_Account: string
            ResponseAction: 'Response_Action_Agree' | ' Response_Action_AgreeAndAdd'
        }
    }>
    /**删除好友申请 */
    deletePendency: IMFunction<{
        From_Account: string
        PendencyType: 'Pendency_Type_ComeIn'
        To_Account: string[]
    }>
    /**获取我的好友等 */
    getAllFriend: IMFunction<{
        From_Account: string
        TimeStamp: number
        StartIndex: number
        GetCount: number
        LastStandardSequence: number
        TagList: string[]
    }>
    /**删除好友 */
    deleteFriend: IMFunction<{
        From_Account: string
        To_Account: string
        DeleteType: 'Delete_Type_Both' | 'Delete_Type_Single'
    }>
    /**增加黑名单 */
    addBlackList: IMFunction<{
        From_Account: string
        To_Account: string
        DeleteType: 'Delete_Type_Both' | 'Delete_Type_Single'
    }>
    /**我的黑名单 */
    getBlackList: IMFunction<{
        From_Account: string
        StartIndex: number
        MaxLimited: number
        LastSequence: number
    }>
    /**删除黑名单 */
    deleteBlackList: IMFunction<{
        From_Account: string
        To_Account: string[]
    }>
    /**创建群 */
    createGroup: IMFunction<{
        GroupId: string
        Owner_Account: string
        Type: any
        Name: string
        FaceUrl: any
        Notification: any
        Introduction: any
        MemberList: string[]

    }>
    /**申请加群等 */
    applyJoinGroup: IMFunction<{
        GroupId: string
        ApplyMsg: string
        UserDefinedField: string
    }>
    /**处理申请加群（同意或拒绝） */
    handleApplyJoinGroup: IMFunction<{
        /**群id*/
        GroupId: any
        /**申请人id*/
        Applicant_Account: any
        /** 是否同意,Agree-同意 Reject-拒绝*/
        HandleMsg: any
        /**申请凭证（包含在管理员收到的加群申请系统消息中）*/
        Authentication: any
        /**消息key（包含在管理员收到的加群申请系统消息中）*/
        MsgKey: any
        /**处理附言 */
        ApprovalMsg: any
        /**用户自定义字段（包含在管理员收到的加群申请系统消息中）*/
        UserDefinedField: any
    }>
    /**删除加群申请 */
    deleteApplyJoinGroupPendency: IMFunction<{
        DelMsgList: {
            From_Account: any
            MsgSeq: any
            MsgRandom: any
        }[]
    }>
    /**主动退群 */
    quitGroup: IMFunction<{
        GroupId: any
    }>
    /**解散群 */
    destroyGroup: IMFunction<{
        GroupId: any
    }>
    /**我的群组列表 */
    getJoinedGroupListHigh: IMFunction<{
        Member_Account: any
        Limit?: any
        Offset?: any
        GroupBaseInfoFilter?: string[]
        SelfInfoFilter?: string[]
    }>
    /**读取群详细资料 */
    getGroupInfo: IMFunction<{
        GroupIdList: string[]
        GroupBaseInfoFilter?: string[]
        MemberInfoFilter?: string[]
    }>
    /**修改群基本资料 */
    modifyGroupBaseInfo: IMFunction<{
        GroupId: string
        Name: string
        Notification: string
        Introduction: string
        ShutUpAllMember: 'On' | 'Off'
    }>
    /**群成员管理 */
    getGroupMemberInfo: IMFunction<{
        GroupId: string
        Offset: 0
        Limit: number
        MemberInfoFilter: string[]
    }>
    /**邀请好友加群 */
    addGroupMember: IMFunction<{
        GroupId: string
        MemberList: { Member_Account: string }[]
    }>
    /**修改群消息提示 修改群消息提示*/
    modifyGroupMember: IMFunction<{
        GroupId: string
        Member_Account: string
        MsgFlag?: any
        Role?: any
    }>
    /**设置群成员禁言时间*/
    forbidSendMsg: IMFunction<{
        GroupId: string
        Member_Account: string
        ShutUpTime: number
    }>
    /**删除群成员*/
    deleteGroupMember: IMFunction<{
        GroupId: string
        MemberToDel_Account: string
    }>

    /**退出，用于切换帐号登录 */
    logout(options, cbOk: (ok) => void, cbErr: (err) => void): void;
    /**上传图片 */
    uploadPic(opts: {
        file: File
        onProgressCallBack?(process: number): void
        From_Account: string
        To_Account: string
        businessType: any
    }, cbOk: (ok) => void, cbErr: (err) => void)
    /** 拉取最新C2C消息 */
    syncMsgs(cbOk: (ok) => void, cbErr?: (err) => void)
    /**获取最近联系人 */
    getRecentContactList: IMFunction<{ Count: number }, {
        SessionItem?: {
            C2cImage: string
            C2cNick: string
            GroupImage: string
            GroupNick: string
            LastC2cMsgFrom_Account: string
            LastMsg: {
                MsgContent: any
                MsgType: Webim.SESSION_TYPE
            }[]
            MsgGroupFromCardName: string
            MsgGroupFromNickName: string
            MsgGroupFrom_Account: string
            MsgGroupReadedSeq: number
            MsgRandom: number
            MsgSeq: number
            MsgShow: string
            MsgTimeStamp: number
            ToAccount: string
            To_Account: string
            Type: number
            UnreadMsgCount: number
        }[]
    }>
    /**删除最近联系人 1:C2C 2:GROUP */
    deleteChat: IMFunction<{ To_Account: string, chatType: any }>
}
