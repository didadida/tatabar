import Vue from 'vue'
//扩展vue对象，用于全局混入 mixins 的代码提示和编译通过
declare module 'vue/types/vue' {
    // 3. 声明为 Vue 补充的东西 
    interface Vue {
        pages:TPage<any>
        rows:any[]
        loading: boolean
        haveMore:any
        pageNo:number
        onReachBottom():void
        onPullDownRefresh():void
    }
} 

declare module 'vue/types/options' {
    interface ComponentOptions<V extends Vue> {
        onPullDownRefresh?():void
    }
}
