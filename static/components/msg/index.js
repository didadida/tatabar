Component({
    data: {
        isHidden: true,
        msg:''
    },
    methods: {
        show(msg){
            this.setData({
                isHidden: false,
                msg
            })
        },
        cancel() {
            this.setData({
                isHidden: true
            })
        }
    }
})