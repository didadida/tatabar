import {Vue,Component} from 'vue-property-decorator'
//主要是分页功能封装
@Component
export default class Global extends Vue {
    loading=true
    pageNo=1
    rows:any[]=[]
    $msg:any=null
    pages={
        limit:0,
        page:0,
        total:0,
        rows:[]
    }
    get haveMore(){
        return this.pages.limit * this.pages.page < this.pages.total
    }
    beforeMount() {
        // setTimeout(() => {
        //     console.log(this.$mp)
        //     // this.$msg = this.$mp.page.selectComponent('#msg')
        // }, 2000);
    }
}