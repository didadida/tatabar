export default {
    getUserInfo(): Promise<any> {
        return new Promise((resolve, reject) => {
            wx.getUserInfo({
                success(rs) {
                    resolve(rs)
                },
                fail() {
                    wx.redirectTo({
                        url: "/pages/login/main"
                    })
                }
            })
        })
    },
    
}