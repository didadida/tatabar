//app.ts
import { Vue, Component } from 'vue-property-decorator'
import im from '@/utils/im'
declare module "vue/types/vue" {
    interface Vue {
        $mp: any;
        getOpenerEventChannel(): {on:any,emit(eventName:string,data:any):void}
    }
}

@Component({
    mpType: 'app', // mpvue特定
} as any)

export default class App extends Vue {
    // app hook
    onLaunch() {
        // wx.im = im(this.$store)
        wx.removeStorageSync("store")
        console.log("小程序实例化完成")
    }
    onShow() {
        console.log('onShow')
    }

    onHide() {
        console.log('onHide')
    }
}