import { Vue, Component } from 'vue-property-decorator'
import { productList, goodsList, ssr, toWeixinPay } from '$api'
@Component
export default class Index extends Vue {
    tabs = ['道具', '酒水']
    activeIndex = 0
    store = {
        height: 0,
        hh: 0,
        gifts: [[{ productName: 'unkown' }]],
        goods: [[{}]]
    }
    current:string = ""
    count: number = 1
    flag = false//是否第一次进入酒水。
    countAdjust(n: number) {
        console.error(n)
        this.count = Math.max(1, this.count + n)
    }
    tabClick(e) {
        if (this.activeIndex == e.currentTarget.id) return;
        this.activeIndex = Number(e.currentTarget.id as any);
        this.current = ""
        if (this.flag) return;
        this.flag = true
    }
    successCallback(goods) {
        wx.showTip(`购买成功`)
        this.$store.state.gift = {
            name: goods.productName,
            count: this.count,
            url: goods.imageUrl
        }
        wx.navigateBack()
    }
    buy() {
        const i: number = Number(this.current[0]), j = Number(this.current[1])
        const list = this.activeIndex == 0 ? this.store.gifts : this.store.goods
        if (!list[i]) return;
        const goods: any = list[i][j]
        toWeixinPay({
            productId: goods.productId,//商品id
            type: this.activeIndex,//1实物，0或空道具
            purchaseNum: this.count,//购买数量
            destIdentifier: this.$store.state.currentChatWith.identifier//接受者identifier,唯一标识
        }).then(data => {
            if (data.paySuccess == '1') {
                this.successCallback(goods)
            } else {
                const self = this
                wx.requestPayment({
                    'timeStamp': data.timeStamp,
                    'nonceStr': data.nonceStr,
                    'package': data.package,
                    'signType': data.signType,
                    'paySign': data.paySign,
                    'success'() {
                        self.successCallback(goods)
                    },
                    'fail'(res) {
                        wx.showTip(`支付失败,可在我的订单或我的酒水中继续支付`, 3000)
                    }
                })
            }
        })
    }
    beforeMount() {
        const restore = wx.getStorageSync("store")
        if (restore) {
            this.store = JSON.parse(restore)
            this.loading = false
            return
        }
        const query = wx.createSelectorQuery();
        query.selectAll('.query').boundingClientRect()
        query.exec(rect => {
            this.store.hh = rect[0][0].height
            this.store.height = wx.getSystemInfoSync().windowHeight - (this.store.hh + rect[0][1].height) - 10
        })
        setTimeout(() => {
            const query2 = wx.createSelectorQuery();
            query2.select("#item").boundingClientRect().exec(rect => {
                ssr(productList({
                    pageNo: this.pageNo, pageSize: 10000
                }), goodsList({
                    pageNo: this.pageNo,
                    pageSize: 10000,
                    userToken: this.$store.state.userInfo.userToken
                })).then(rs => {
                    const list1=rs[0].rows
                    const list2 = rs[1].rows.map(v => ({
                        imageUrl: v.imageUrl,
                        needCoinNum: v.prePrice,
                        productId: v.goodsId,
                        productName: v.goodsName
                    }))
                    const count = Math.floor(this.store.height / rect[0].height) * 4
                    this.store.gifts = new Array(Math.ceil(list1.length / count)).fill(0).map(v => list1.splice(0, count))
                    this.store.goods = new Array(Math.ceil(list2.length / count)).fill(0).map(v => list2.splice(0, count))
                    console.error(this.store.goods)
                    this.loading = false
                    wx.setStorageSync("store", JSON.stringify(this.store))
                })
            })
        }, 100);

    }
}
