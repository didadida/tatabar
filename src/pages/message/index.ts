import { Vue, Component } from 'vue-property-decorator'

// 必须使用装饰器的方式来指定component
@Component
export default class Index extends Vue {
    x = 0
    y = 0
    curIndex = -1
    n = 0
    get msgList() {
        this.n++
        if (this.n > 1) {
            this.loading = false
        }
        return this.$store.state.msgList
    }
    filterNickName(nickName: string) {
        return nickName.substring(0, 8)
    }
    delChat(index) {
        wx.im.deleteChat({
            To_Account: this.msgList[index].identifier,
            chatType: wx.im.SESSION_TYPE.C2C,
        }, ok => {
            this.$store.commit('schedulerMsgList', { type:'delete',index }) 
            this.curIndex = -1 
        }, fail => this.curIndex = -1)
    }
    touchstart(e, index) {
        if (this.curIndex != index) {
            this.curIndex = -1
        }
        this.x = e.pageX
        this.y = e.pageY
    }
    touchend(e, index) {
        const disx = e.mp.changedTouches[0].pageX - this.x
        const disy = e.mp.changedTouches[0].pageY - this.y
        if (Math.abs(disx) <= Math.abs(disy) || Math.abs(disx) < 30) return;
        if (disx > 0) {
            this.curIndex = -1
        } else {
            this.curIndex = index
        }
    }
    itemClick(item) {
        if (!this.$store.state.logined) return;
        this.curIndex = -1
        this.$store.commit('toggleChatWith', item)
        wx.navigateTo({
            url: `chat/main?user=` + encodeURIComponent(JSON.stringify(item))
        })
    }
}