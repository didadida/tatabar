import { Vue, Component,Prop } from 'vue-property-decorator'
// 必须使用装饰器的方式来指定component

@Component
export default class Index extends Vue {
    @Prop(Boolean)readonly isMe:boolean
    @Prop(Boolean)readonly isText:boolean
    @Prop(String) readonly myHeadUrl:string
    @Prop(String) readonly taHeadUrl:string
    @Prop({default:''}) readonly msg:Object|string
    @Prop(Boolean) readonly loaded:boolean
    @Prop(Boolean) readonly failed:boolean
    @Prop(Boolean) readonly id:string
    @Prop(Number) readonly time:number

    resend(){
        this.$emit('resend', this.time,this.msg)
    }
}
