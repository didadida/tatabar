import { Vue, Component, Watch } from 'vue-property-decorator'
import { filterMsgList, pageToBottom, filterMsg } from '@/utils/im.util'
import ChatItem from '../chatItem/index'
import Goods from '../goods/index'
interface Gift { url: string, name: string, count: number }
@Component({
    components: {
        ChatItem,
        Goods
    }
})
export default class Index extends Vue {
    lastTime: number = 0
    more: boolean = true
    myHeadUrl = this.$store.state.userInfo.thumbnail
    content: string = ""
    errList: Msg[] = []
    get profile(): MsgItem {
        return this.$store.state.currentChatWith
    }
    get userInfo() {
        return this.$store.state.userInfo
    }
    pageToBottom() {
        pageToBottom("#chat")
    }
    getHistory(isPullDown: boolean = false) {
        if (!this.more) return;
        wx.showNavigationBarLoading()
        if (!this.$store.state.logined) return;
        wx.im.getC2CHistoryMsgs({
            'Peer_Account': this.profile.identifier, //好友帐号，selToID 为全局变量，表示当前正在进行的聊天 ID，当聊天类型为私聊时，该值为好友帐号，否则为群号。
            'MaxCnt': 15, //拉取消息条数
            'LastMsgTime': this.lastTime, //最近的消息时间，即从这个时间点向前拉取历史消息
            'MsgKey': ""
        }, ok => {
            this.more = ok.Complete != 1
            this.lastTime = ok.LastMsgTime
            this.rows = filterMsgList(ok.MsgList).concat(this.rows)
            !isPullDown && this.pageToBottom()
            wx.hideNavigationBarLoading()
        }, err => {
            wx.showTip(err.ErrorInfo)
            wx.hideNavigationBarLoading()
        })
    }
    receiveMsg(msg: Msg) {
        this.rows.push(filterMsg(msg, true))
    }
    sendMsg(arg: string | Gift | Msg) {
        let msg
        if ((arg as Msg).getTime) {
            //如果参数已经是msg对象。即重新发送。直接发送msg
            msg = arg
        } else {
            let message
            if (typeof arg === 'object') {
                //礼物酒水消息
                message = arg
            } else {
                //文字消息
                message = (arg || this.content).trim()
                let msglen = message.length
                if (msglen < 1) return wx.showTip('不能发送空消息');
                if (wx.im.Tool.getStrBytes(message) > wx.im.MSG_MAX_LENGTH.C2C) return wx.showTip('消息长度超出限制')
            }
            //目前不考虑群聊
            let sess = wx.im.MsgStore.sessByTypeId(wx.im.SESSION_TYPE.C2C, this.profile.identifier)
            if (!sess) {
                //生成新的session对象 
                sess = new wx.im.Session(wx.im.SESSION_TYPE.C2C, this.profile.identifier, this.profile.identifier, this.profile.identifierNick, Math.round(Date.now() / 1000))
            }
            //生成新的MSG对象
            msg = new wx.im.Msg(sess, true, -1, -1, -1, this.userInfo.userToken, (typeof arg === 'object') ? 0 : (wx.im.C2C_MSG_SUB_TYPE.COMMON as any), this.userInfo.nickName)
            let elemText
            if (typeof arg === 'object') {
                elemText = new wx.im.Msg.Elem.Custom((arg as Gift).name, (arg as Gift).url, (arg as Gift).count.toString())
                msg.addCustom(elemText)
            } else {
                elemText = new wx.im.Msg.Elem.Text(message)
                msg.addText(elemText)
            }
        }

        let showItem = filterMsg(msg, true)
        this.rows.push(showItem)
        wx.im.sendMsg(msg, ok => {
            showItem.loading = false
            showItem = msg = null as any
        }, err => {
            showItem.loading = false
            showItem.failed = true
            this.errList.push(msg)
        })
        this.pageToBottom()
        this.content = ""
    }
    resend(time, msg) {
        this.rows.splice(this.rows.findIndex(v => v.timeStamp == time), 1)
        msg = this.errList.splice(this.errList.findIndex(v => v.getTime() == time), 1)
        this.sendMsg(msg[0])
    }
    onShow() {
        this.getHistory()
        wx.setTitle(this.profile.identifierNick.substring(0, 20))
        if (this.$store.state.gift) {
            this.sendMsg(this.$store.state.gift)
            this.$store.state.gift = null
        }
        wx.im.setAutoRead(wx.im.MsgStore.sessByTypeId(wx.im.SESSION_TYPE.C2C, this.profile.identifier), true, true)
        wx.onMessage = (msg: Msg): void => {
            this.rows.push(filterMsg(msg))
            this.pageToBottom()
        }
        const index = this.$store.state.msgList.findIndex(v => v.identifier == this.profile.identifier)
        this.$store.commit('schedulerMsgList', { type: 'update', index, data: { unread: 0 } })
    }
    onHide() {
        console.error(2)
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        if (!this.more) return
        this.getHistory(true)
    }
}
