import { Vue, Component } from 'vue-property-decorator'
import Type from './type.vue'
// import Bg from './bg'
import { giftRankDetail } from '$api'

// 必须使用装饰器的方式来指定component
@Component({
    components: {
        type: Type
    }
})
export default class Crunchies extends Vue {
    tabs = ["收礼榜", "送礼榜"]
    activeIndex = 0
    sendRank: GiftRank = {
        nickName: this.$store.state.userInfo.nickName,
        userImage: this.$store.state.userInfo.thumbnail,
        rank: 0,
        coins: 0,
        sex: this.$store.state.userInfo.sex,
        topList: []
    }
    getRank: GiftRank = {
        nickName: this.$store.state.userInfo.nickName,
        userImage: this.$store.state.userInfo.thumbnail,
        rank: 0,
        coins: 0,
        sex: this.$store.state.userInfo.sex,
        topList: []
    }
    flag: boolean = true
    tabClick(e) {
        this.activeIndex = Number(e.currentTarget.id);
        if (!this.flag) return;
        this.flag = false
        this.getList()
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.getList()
    }
    getList() {
        giftRankDetail({
            clubUniq: this.$store.state.clubUniq,
            rankType: this.activeIndex + 1//1收礼榜2送礼榜
        }).then(rs => {
            this[this.activeIndex == 0 ? 'getRank' : 'sendRank'] = rs
        })
    }
    beforeMount() {
        this.getList()
    }
}
