interface Coord {
    x: number //x坐标
    y: number  //y坐标
}

interface Dot extends Coord {
    r: number //半径
    s: number  //闪烁速度
    sc:number  //闪烁幅度
}

interface Meteor extends Coord {
    len: number //长度
    s: number  //运动速度
}

function createCoord(w: number, h: number): Coord {
    return {
        x: Math.floor(w * Math.random()),
        y: Math.floor(h * Math.random()),
    }
}


export default class Bg {
    dots: Dot[] = []
    meteors: Meteor[] = []
    readonly dotNum: number = 50
    readonly meteorNum: number = 10
    readonly lineWidth: number = 2
    readonly angle: number = Math.sin(-45 / 180 * Math.PI)
    constructor(private ctx: wx.CanvasContext, private ww: number, private wh: number) { }
    private createDot(): Dot {
        let coord = createCoord(this.ww, this.wh) as Dot
        coord.r = Math.floor(3 * Math.random())
        coord.s = Math.floor(3 * Math.random())
        coord.sc = Math.floor(10 * Math.random())
        return coord
    }
    private createMetor(): Meteor {
        return {
            x: Math.floor(this.ww*2 * Math.random()),
            y: Math.floor(-100 * Math.random()),
            len: Math.floor(20 + 50 * Math.random()),
            s: Math.floor(5 + 10 * Math.random()),
        }
    }

    drawDot(v: Dot) {
        v.s = (v.s + v.sc) % 360
        const r = v.r * Math.max(0, Math.sin(v.s / 180 * Math.PI))*1.2
        this.ctx.setShadow(0, 0, 20, '#fff')
        this.ctx.arc(v.x, v.y, r, 0, Math.PI * 2)
        this.ctx.closePath()
    }

    drawMeteor(v: Meteor, i: number) {
        if (v.x <0 && v.y > this.wh) {
            this.meteors[i] = this.createMetor()
        }
        v.x -= v.s
        v.y += v.s
        const sx = v.x + v.len * this.angle
        const sy = v.y - v.len * this.angle
        this.ctx.beginPath()
        this.ctx.lineTo(v.x, v.y)
        this.ctx.lineTo(sx, sy)
        const grd = this.ctx.createLinearGradient(v.x, v.y, sx - this.lineWidth, sy)
        grd.addColorStop(1, 'white')
        grd.addColorStop(0, 'transparent')
        this.ctx.setStrokeStyle(grd as any)
        this.ctx.stroke()
    }

    drawAll() {
        this.ctx.save()
        this.ctx.clearRect(0, 0, this.ww, this.wh)
        this.ctx.setFillStyle("#ccc")
        this.dots.forEach(this.drawDot.bind(this))
        this.ctx.closePath()
        this.ctx.fill()
        this.ctx.restore()

        this.meteors.forEach(this.drawMeteor.bind(this))
        this.ctx.draw()
    }
    public init() {
        for (let i = 0; i < this.dotNum; i++) {
            this.dots.push(this.createDot())
        }
        for (let i = 0; i < this.meteorNum; i++) {
            this.meteors.push(this.createMetor())
        }
        this.ctx.setLineWidth(this.lineWidth)
        this.ctx.setLineCap("round")
        this.drawAll()
        setInterval(()=>{
            this.drawAll()
        }, 60)
    }
}