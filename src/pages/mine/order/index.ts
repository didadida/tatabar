import { Vue, Component } from 'vue-property-decorator'
import { ordersList, wxcancelOrder, confirmToPay } from '$api'
// import { payment } from '@/http/config'
@Component
export default class Index extends Vue {
    tabs = ["已完成", "待支付", "已取消"]
    activeIndex: number = 0
    tabClick(e) {
        if (this.activeIndex == e.currentTarget.id) return;
        this.activeIndex = Number(e.currentTarget.id as any);
        this.pageNo = 1
        this.getOrderByType()
        wx.pageScrollTo({
            scrollTop: 0
        })
    }
    getOrderByType() {
        this.loading = true
        ordersList({ statusType: (this.activeIndex === 0 ? 1 : (this.activeIndex === 1 ? 0 : 2)), pageNo: this.pageNo })
            .then(rs => {
                this.pages = rs
                this.rows = (this.pageNo == 1 ? [] : this.rows).concat(rs.rows)
            }).finally(() => {
                this.loading = false
            })
    }
    onReachBottom() {
        if (this.loading || !this.haveMore) return;
        this.pageNo++
        this.getOrderByType()
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.getOrderByType()
    }
    cancel(item) {
        wx.modal('确认取消此订单吗').then(() => {
            wx.loading()
            wxcancelOrder({ orderId: item.ordersId }).then(rs => {
                wx.showTip(`订单取消成功`)
                item.statusType = 2
            }).finally(wx.hideLoading)
        })
    }
    payment(item: OrderList["rows"][0]) {
        wx.loading()
        confirmToPay({
            orderId: item.ordersId,
            type: 0
        }).then(rs => {
            if (rs.paySuccess == 1) {
                return wx.showTip(`订单已支付成功，请勿重新提交`)
            }
            wx.requestPayment(Object.assign(rs, {
                success(res) {
                    wx.showTip(`支付成功`)
                    item.statusType = 1
                },
                fail(err: any) {
                    wx.showTip(`支付失败:${err.err_desc}`)
                },
                compelete() {
                    wx.hideLoading()
                }
            }) as any)
        }).finally(wx.hideLoading)
    }
    beforeMount() {
        this.getOrderByType()
    }
}