import { Vue, Component } from 'vue-property-decorator'
import { coinTransInfo, coinTransList, mnGetLoginSession, ssr } from '$api'
// 必须使用装饰器的方式来指定component
@Component
export default class Index extends Vue {
    info: coinTrans = {
        amount: 0,
        successfulWithdrawalsDto: 0,
        presentDto: 0,
        isBindWithdraw: 2,
    }
    sessionId: any = ''
    author = false
    withdrawal() {
        if (this.loading) return;
        if (this.info.amount == 0) {
            return wx.showTip(`金币余额不足`)
        }
        if (this.info.isBindWithdraw != 1) {
            wx.loading('正在请求权限')
            mnGetLoginSession().then(rs => {
                wx.navigateTo({ url: 'authour/main?sessionId=' + rs.sessionId + '&coin=' + this.info.amount })
            }).finally(wx.hideLoading)
            return
        }
        wx.navigateTo({
            url: 'coinOut/main?coin=' + this.info.amount
        })
    }
    onReachBottom() {
        if (this.loading || !this.haveMore) return;
        this.loading = true
        this.pageNo++
        coinTransList({ pageNo: this.pageNo })
            .then((rs) => {
                this.rows = this.rows.concat(rs.rows)
            }).finally(() => this.loading = false)
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.fetch()
    }
    fetch() {
        this.loading = true
        ssr(coinTransInfo(), coinTransList({ pageNo: this.pageNo }))
            .then((rs) => {
                rs[0].isBindWithdraw = 0
                this.info = rs[0]
                this.pages = rs[1]
                this.rows = (this.pageNo == 1 ? [] : this.rows).concat(rs[1].rows)
            }).finally(() => this.loading = false)
    }
    onShow() {
        this.fetch()
    }
}