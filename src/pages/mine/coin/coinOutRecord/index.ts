import { Vue, Component } from 'vue-property-decorator'
import { formatTime } from '@/utils'
import { coinWithdrawalList } from '$api'
@Component
export default class Index extends Vue {
    end = formatTime(new Date(), "YYYY-MM")
    date = formatTime(new Date(), "YYYY-MM");
    status = 1
    bindDateChange(e) {
        console.log('picker发送选择改变，携带值为', e.mp.detail.value)
        this.date = e.mp.detail.value
        this.getList()
    }
    getList() {
        this.loading = true
        coinWithdrawalList({
            status: this.status,
            date: this.date
        }).then(rs => {
            this.pages = rs
            rs.rows.forEach(v=>v.moneyDto=v.money.toFixed(2))
            this.rows = (this.pageNo == 1 ? [] : this.rows).concat(rs.rows)
        }).finally(() => this.loading = false)
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.getList()
    }
    onReachBottom() {
        if (!this.haveMore) return;
        this.pageNo++
        this.getList()
    }
    beforeMount() {
        this.status = Number(this.$mp.query.status)
        wx.setTitle(this.status == 1 ? '待提现金币' : '已提现金币')
        this.getList()
    }
}