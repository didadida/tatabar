import { Vue, Component } from 'vue-property-decorator'
import { baseUrl } from '@/http/config'
// 必须使用装饰器的方式来指定component
@Component
export default class Index extends Vue {
    flag=false
    sessionId=""
    baseUrl = baseUrl
    coin=0
    message(rs){
        if (rs.mp.detail.data.code==1){
            wx.navigateTo({
                url: 'coinOut/main?coin=' + this.coin
            })
        }else{
            wx.showTip(rs.mp.detail.data.message)
        }
    }
    beforeMount() {
        this.sessionId = this.$mp.query.sessionId
        this.coin = this.$mp.query.coin
        this.flag=true
    }
}