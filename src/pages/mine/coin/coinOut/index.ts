import { Vue, Component } from 'vue-property-decorator'
import { toWeixinWithdrawal } from '$api'

// 必须使用装饰器的方式来指定component
@Component
export default class Index extends Vue {
    currentMoney = 0
    withdrawalMoney = 0
    withdrawal() {
        if(this.currentMoney==0){
            return wx.showTip(`金币余额不足`)
        }
        if(this.currentMoney<this.withdrawalMoney){
            return wx.showTip(`提现金额超出余额`)
        }
        toWeixinWithdrawal({ coinNum: this.withdrawalMoney }).then(rs=>{
            this.currentMoney = rs.amount
            wx.showTip(`提现申请成功，请等待审核结果`)
        }).finally(wx.hideLoading);
    }
    beforeMount() {
        this.currentMoney  = this.$mp.query.coin
    }
}