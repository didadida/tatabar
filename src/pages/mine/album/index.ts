import { Vue, Component } from 'vue-property-decorator'
import { delUsersAlbum, updateUsersAlbum } from '$api'

@Component
export default class Index extends Vue {
    albumList: { id: number, imageUrl: string }[] = []
    height = (wx.getSystemInfoSync().windowHeight - 60) + 'px'
    targetSrc: string = ""
    showMask: boolean = false
    current: number = -1
    status=false
    get style() {
        return `position:fixed;left:110vw;top:0;z-index:-1;width:${this.$store.state.canwidth}px;height:${this.$store.state.canheight}px`
    }
    get emptyCount() {
        return 6 - this.albumList.length
    }
    del(id, index) {
        wx.modal("确认删除该图片吗").then(rs => {
            wx.loading()
            delUsersAlbum({ id }).then(rs => {
                this.albumList.splice(index, 1)
            }).finally(wx.hideLoading)
        })
    }
    preivew(e, index, url) {
        e.stopPropagation()
        if (this.current != -1) {
            const from = this.albumList[this.current]
            const to = this.albumList[index]
            this.$set(this.albumList, this.current, to)
            this.$set(this.albumList, index, from)
        } else {
            wx.previewImage({
                current: '',
                urls: [url]
            })
        }
    }
    updateUsersAlbum() {
        updateUsersAlbum({
            albumSortArr: JSON.stringify(this.albumList.map((v, i) => ({ "id": v.id, "sort": i })))
        }).then(rs => wx.showTip(`保存成功`))
    }
    addAlbum() {
        const self = this
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: (res) => {
                self.targetSrc = res.tempFilePaths[0]
                self.showMask = true
            }
        })
    }
    uploading() {
        if (!this.targetSrc || this.status) return;
        wx.loading(`请等待上传完成`)
        this.status = true
        wx.uploadFile2({
            filePath: this.targetSrc,
            canvasId: 'compress',
            type: '1',
        }).then(rs => {
            this.showMask = false
            this.albumList.unshift(rs)
        }).catch(err => wx.showTip(err))
            .finally(() => {
                wx.hideLoading()
                this.status = false
            })
    }
    onShow() {
        this.albumList = this.$store.state.userInfo.usersAlbums
    }
}
