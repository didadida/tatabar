import { Vue, Component } from 'vue-property-decorator'
import { coinTransList } from '$api'
@Component
export default class Index extends Vue {
    productId=-1
    getList(){
        this.loading=true
        coinTransList({
            productId: this.productId,
            pageNo:this.pageNo
        }).then(rs=>{
            this.pages = rs
            this.rows = (this.pageNo==1?[]:this.rows).concat(rs.rows)
        }).finally(()=>this.loading=false)
    }
    onReachBottom() {
        if (this.loading || !this.haveMore) return;
        this.pageNo++
        this.getList()  
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.getList()
    }
    beforeMount() {
        this.productId = Number(this.$mp.query.id)
        this.getList()   
    }
}

