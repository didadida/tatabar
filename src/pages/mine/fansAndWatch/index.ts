import { Vue, Component } from 'vue-property-decorator'
import { followList, follow } from '$api'

@Component
export default class Index extends Vue {
    current = -1
    type: 1 | 2 = 1
    convert(item: User) {
        //对方没有关注我
        if (item.status != 2) {
            item.status = 1
        }
    }
    getList() {
        this.loading = true
        followList({ type: this.type, pageNo: this.pageNo })
            .then(rs => {
                (this.pages as FansAndFollow) = rs
                if (this.type == 1) {
                    //如果是我的粉丝。那么对status进行转换以同步我的关注逻辑。
                    this.pages.rows.forEach(this.convert)
                }
                this.rows = (this.pageNo == 1 ? [] : this.rows).concat(this.pages.rows)
            }).finally(() => this.loading = false)

    }
    action(item: User) {
        const self = this
        if (item.status != 1) {
            wx.modal("确认取消关注吗").then(rs => self.next(item))
        } else {
            self.next(item)
        }
    }
    next(item: User) {
        follow({
            type: item.status == 1 ? 1 : 2,
            followUserId: item.userId!
        }).then(rs => {
            wx.showTip(`${item.status == 1 ? '关注' : '取关'}成功`)
            item.status = rs.status
        })
    }
    onReachBottom() {
        if (this.loading || !this.haveMore) return;
        this.pageNo++
        this.getList()
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.getList()
    }
    onShow() {
        this.type = Number(this.$root.$mp.query.type) as any
        wx.setTitle('我的' + (this.type == 1 ? '粉丝' : '关注'))
        this.getList()
    }
}
