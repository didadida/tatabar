import { Vue, Component } from 'vue-property-decorator'
import { formatTime } from '@/utils'
import { editInfo, userEdit } from '$api'
// 必须使用装饰器的方式来指定component
@Component
class Index extends Vue {
    end = formatTime(new Date(), "YYYY-MM-DD")
    date = "2017-01-01";
    loading = true
    userInfo: UserInfo = {sex:1}
    bindDateChange(e) {
        console.log('picker发送选择改变，携带值为', e.mp.detail.value)
        this.userInfo.birthday = e.mp.detail.value
    }
    actionSheet() {
        var self = this
        wx.showActionSheet({
            itemList: ['查看大图', '更改头像'],
            success: function (res) {
                if (res.tapIndex == 0) {
                    wx.previewImage({
                        current: '',
                        urls: [self.userInfo.userImage!]
                    })
                } else {
                    wx.navigateTo({
                        url: '/pages/upload/main'
                    })
                }
            }
        });
    }
    save() {
        wx.loading()
        let body = {
            nickName: this.userInfo.nickName,
            sex: this.userInfo.sex,
            hobby: this.userInfo.hobby,
            birthday: this.userInfo.birthday,
            customSign: this.userInfo.customSign
        }
        for(var i in body){
            console.error(i,body[i])
            if(!body[i]){
                return wx.showTip(`资料填写不完整`)
            }
        }
        userEdit(body).then(rs => {
            this.$store.commit('updateUserInfo',body)
            wx.navigateBack()
            wx.showTip('修改成功')
            wx.hideLoading()
        })
    }
    onShow() {
        this.userInfo = Object.assign({}, this.$store.state.userInfo)
    }
    beforeMount() {
        editInfo().then(rs => {
            this.userInfo = rs
            this.$store.commit('updateUserInfo', rs)
        })
    }
}

export default Index