import { Vue, Component } from 'vue-property-decorator'
import { goodsOrdersList, wxcancelOrder, confirmToPay } from '$api'
type Item = DrinkOrder["rows"][0]
@Component
export default class Index extends Vue {
    tabs = ["送出", "收到"]
    activeIndex: 0 | 1 = 0
    toString(statusType): string {
        switch (Number(statusType)) {
            case 0: return '待支付'
            case 1: return (this.activeIndex == 0 ? '已支付, 待确认' : '待配货')
            case 2: return '已取消'
            case 3: return '已配送'
            case 6: return '已完成'
            case 7: return '已退款'
            default: return ''
        }
    }
    tabClick(e) {
        if (this.activeIndex == e.currentTarget.id) return;
        this.activeIndex = e.currentTarget.id;
        this.pageNo = 1
        wx.pageScrollTo({
            scrollTop: 0
        })
        this.getOrderByType()
    }
    cancel(item: Item) {
        wx.modal("是否取消该订单").then(rs => {
            wx.loading()
            return wxcancelOrder({ orderId: item.ordersId })
        }).then(rs => {
            item.statusType = 2
            item.statusTypeString = this.toString(2)
        })
    }
    payment(item: OrderList["rows"][0]) {
        var self = this
        wx.loading()
        confirmToPay({
            orderId: item.ordersId,
            type: 1
        }).then(rs => {
            if (rs.paySuccess == 1) {
                return wx.showTip(`订单已支付成功，请勿重新提交`)
            }
            wx.requestPayment(Object.assign(rs, {
                success(res) {
                    wx.showTip(`支付成功`)
                    item.statusType = 1
                    item.statusTypeString = self.toString(1)
                },
                fail(err: any) {
                    wx.showTip(`支付失败:${err.err_desc}`)
                },
                compelete() {
                    wx.hideLoading()
                }
            }) as any)
        }).finally(wx.hideLoading)
    }
    getOrderByType() {
        this.loading = true
        goodsOrdersList({ type: this.activeIndex, pageNo: this.pageNo, pageSize: 3 })
            .then(rs => {
                this.pages = rs
                this.rows = (this.pageNo == 1 ? [] : this.rows).concat(rs.rows)
                this.rows.forEach(v => (v.statusTypeString!) = this.toString(v.statusType))
            }).finally(() => this.loading = false)
    }
    onReachBottom() {
        if (this.loading || !this.haveMore) return;
        this.pageNo++
        this.getOrderByType()
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.getOrderByType()
    }
    beforeMount() {
        this.getOrderByType()
    }
}