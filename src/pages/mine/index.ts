import { Vue, Component } from 'vue-property-decorator'
import {centerData} from '$api'
@Component
export default  class Index extends Vue { 
    get remaining():number{
        return 6 - this.user.usersAlbums.length
    }
    get bigImages():string[]{
        return this.user.usersAlbums.map(v=>v.imageUrl)
    }
    user: MineDetal={ 
        thumbnail: this.$store.state.userInfo.thumbnail,
        userImage: this.$store.state.userInfo.userImage,
        sex: this.$store.state.userInfo.sex,
        nickName: this.$store.state.userInfo.nickName,
        amount:0,
        fansDto:0,
        phone:0,
        usersAlbums:[],
        combos:[]
    }
    async getData(){
        this.loading=true
        this.user = await centerData()
        this.$store.commit('updateUserInfo', this.user)
        this.loading = false
    }
    viewInfo(index){
        wx.previewImage({
            current:this.user.usersAlbums[index].thumbnail,
            urls:this.bigImages
        })
    }
    onShow(){
        Object.assign(this.user,this.$store.state.userInfo)
    }
    onPullDownRefresh(){
        wx.stopPullDownRefresh()
        this.getData()
    }
    async beforeMount() {
       this.getData()
    }
}