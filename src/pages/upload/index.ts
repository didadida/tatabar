import { Vue, Component } from 'vue-property-decorator'
import MpvueCropper from 'mpvue-cropper/mpvue-cropper.vue'

const device = wx.getSystemInfoSync()
const width = device.windowWidth
const height = width
const innerWidth = width * 0.8
let wecropper
@Component({
    components: {
        MpvueCropper
    }
})
export default class Index extends Vue {
    height = (device.windowHeight - 60) + 'px'
    showMask = false
    targetSrc = null
    flag = false
    cropperOpt = {
        id: 'cropper', // 用于手势操作的canvas组件标识符
        targetId: 'targetCropper', // 用于用于生成截图的canvas组件标识符
        pixelRatio: device.pixelRatio, // 传入设备像素比
        width,  // 画布宽度
        height, // 画布高度
        scale: 2.5, // 最大缩放倍数
        zoom: 8, // 缩放系数
        cut: {
            x: (width - innerWidth) / 2, // 裁剪框x轴起点
            y: (width - innerWidth) / 2, // 裁剪框y轴期起点
            width: innerWidth, // 裁剪框宽度
            height: innerWidth // 裁剪框高度
        },
        boundStyle: {
            color: '#04b00f',
            mask: 'rgba(0,0,0,0.8)',
            lineWidth: 1
        }
    }
    get style() {
        return `position:fixed;left:110vw;top:0;z-index:-1;width:${this.$store.state.canwidth}px;height:${this.$store.state.canheight}px`
    }

    uploadTap() {
        var self = this
        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: (res) => {
                const src = res.tempFilePaths[0]
                //  获取裁剪图片资源后，给data添加src属性及其值
                wecropper.pushOrigin(src)
                self.flag = true
            }
        })
    }
    getCropperImage() {
        if (!this.flag) return;
        var self = this
        this.loading=false
        wx.showLoading({ title: '正在生成图片...' })
        wecropper.getCropperImage({ original: true })
            .then((src) => {
                console.error(src)
                self.targetSrc = src
                this.showMask = true
                this.loading = true
                wx.hideLoading()
            }).catch(e => {
                console.error('获取图片失败')
                wx.hideLoading()
                this.loading = true
            })
    }
    emptyFunc(cb) {
        cb()
    }
    uploading() {
        if (!this.targetSrc) return;
        wx.loading(`请等待上传完成`)
        this.loading = false
        wx.uploadFile2({
            filePath: this.targetSrc,
            canvasId: 'compress',
            type: 2
        }).then(rs => {
            console.error(rs)
            this.$store.state.userInfo.thumbnail = this.targetSrc
            this.$store.state.userInfo.userImage = this.targetSrc
            wx.navigateBack()
        }).catch(err => {
            wx.showTip("图片上传失败")
        }).finally(() => {
            wx.hideLoading()
            this.loading=true
        })
    }
    mounted() {
        wecropper = this.$refs.cropper
    }
}
