import { Vue, Component } from 'vue-property-decorator'
@Component
export default class Index extends Vue {
    map: any = null
    get lat() {
        return this.$mp.query.lat * 1
    }
    get lng() {
        return this.$mp.query.lng * 1
    }
    get markers() {
        return [{
            iconPath: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowMjE0ODMxMUIxRDAxMUU5QUQxM0EwMUM1RDVDOTA1MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowMjE0ODMxMkIxRDAxMUU5QUQxM0EwMUM1RDVDOTA1MiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjAyMTQ4MzBGQjFEMDExRTlBRDEzQTAxQzVENUM5MDUyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjAyMTQ4MzEwQjFEMDExRTlBRDEzQTAxQzVENUM5MDUyIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+GBXIqgAABaBJREFUeNqsV2lsVFUUPve+mel0ukxLS1va0gJpZV9ScWuNf2xEpC1mCFRFUCuIRKKJQOIPSUw0/tC4kgA1SjUWRKRWSgFDaFxSRAKipZS2iCzd6DJ1us2003lzr+feO9NppaTT5SSTyXv3vfOd+92zfI9wziEIM/I6+0p+vimb/21fytucqdDjjgKCK5HmLhIf1kDmxlaR+5MrSFrMSbzrHsshGQM4nJXV7WCHq19kF5qTocMJQBHNbABiNsoHeL8HYEBXzuLCAMFv06eWfEEfT38fb/WMG5hf68z3vnn6Y3bqWgKYNCCJERJQLf7fi++/XwfejFiMA101t117J3s7mWktDhqYlV75RH/95KvQMwAkPRZAI9JZUCYY0Rnwq3YAZMDwac4+uiJ965jA7Ks/D+mvHMsnM3CHCeHSyZBDtxe4wwXgRHq9vvdEUOEmINGhIJgZCtBA1e47XWDYbyulaxba7grMfry6W7d9s43MiQaIQkdephwLwFtdmEghQJfNADJvOsB0i6Icz51daQde1YoBDQJJxXdNVAWm4X+nE3hLLxiPbywkD6e+fAcwb+rO82R9dhRMmDjx4QrUqOFLGLXLAzR/MdD1S4EuTxqVYXa2EVjxX8COXAZiNSu2PEyCy51jMKYzL62D2LDvhgOb9I1HHKy83kIWxil6BehNBzoJAe2DJ4A+lhbUEWMVgL7zpGSJpFgVuKD9UqsIfNCwJw8pAReVD/968w1WVmsh98QoUBGl2GmYCQxHnw0aVKZC3jwwfv+MOqI251CyYZ0DO3zZxP9o3iWfEz9WdHGHoFieiTA31iVmtPHLNUDmTIPxGlkYD8bC1cDtzkByIoOA7HqLLr4mGKa8sXsl+70xgiRFqozECPkNB2jPZQB5IBkmaiQ7Dei6xfK45K7RN0mOBH6mIRQzPZfyyls50NYHEKo6EQx6ASJMQLfcB5M1Tfgw4E49XnXDYpKJxn5ryKG83r4UCBlqR9zuAvrgTMCeO2lgIkpveSKAYyDQ4bBaeG3HIspbe1Mg1BBog6IWF8TBVBmdHwe81x1otaLJtPUlUuh2W4mfZrmIq9MsUwYMMaEj2y1Sz/sGIylun4ycDqNNgUkav/OCgtXcLUfb8CaP5zxl1uFSNe03UdMRpl6Kw6ABBLBvjWDD5zXtU4bLatpk9xtKLlE1CREtlMyfXiXvcB9yjAWwrkXmTZ7h883AL7aogeNnGdsnJm81xYlxgszAhj7go1tkHTLg3Xtu0sDShw9MWh9WTLIVaGbKCUH1CfJQSh9Op0CHmR0N7MAl4D9fnzjFZbVCUACZFR3oiIhBHpnlhihzmQhF1woyPpQ91cOGdk2wDDybfgBe3TZ+0HNNoG8rBykm/ImF00r0a+2FjN3iSnKAO36Xrl00wOs7FC1iiPvUh8d2EFhJTfCgB6pAzz+khgLmi9ytGIt1HUA3LNPJkoS3RiqQdqdtMKuwRICSxEifEKBytHFHP9An5yshkJminA43VJms8pYSAuX1Um1KUN03i1G9CHFgrNy8Hsv34B3Sh/9yY58nt3iLnFSxvhf94k1InxBNjjwhfYhP+vB2p6wAfgWPRMegU6MC4lCw19oH/F8XGE89X0TuTSq4u9grqSnRN5XaRAmIMTZC7Ikc6OoXLU8xIkcQlkcE1mmUWQENF3siWKwQw9dry1Fn544tbyv+2ePdVr6V3+4FqUqM2vjkrRCHKG/J7CjQ9q7+HI9nc/BfEi29G/Rdpz9ix+piZIkJ+i1G1X1GE/RcTTYp7DBQalvgMLydvR3PumginzDR7KfrO9m31QX8bGO8YEBSLJqMvymIoxBtUHQkDI5kpti1p5fsJ1mp7+Fq50S/nfxmQYm0il9ofhRLbjFvdyXj7qzq68rUg3K4CRPuMlmeVIGfOsdFjxrL4X8CDACr2ov+1dQDjgAAAABJRU5ErkJggg==",
            id: 0,
            latitude: this.lat,
            longitude: this.lng,
            width: 25,
            height: 25,
            title: "fasf",
            callout: {
                textAlign: 'center',
                content: decodeURIComponent(this.$mp.query.name),
                fontSize: 16,
                color: "#fff",
                bgColor: "#fa3898",
                display: "ALWAYS",
                padding: 4,
                borderRadius: 6,
            },
        }]
    }
    polyline: any[] = []
    markertap(e) {
        var self = this
        wx.showActionSheet({
            itemList: [decodeURIComponent(this.$mp.query.address), '驾车路线', '步行路线'],
            success(e) {
                if (e.tapIndex === 0) return;
                wx.getLocation({
                    success(rs) {
                        let p1 = {
                            longitude: rs.longitude,
                            latitude: rs.latitude
                        }
                        let p2 = {
                            longitude: self.lng,
                            latitude: self.lat
                        }
                        wx.request({
                            url: `https://apis.map.qq.com/ws/direction/v1/${e.tapIndex === 1 ? 'driving' : 'walking'}/?from=${p1.latitude},${p1.longitude}&to=${p2.latitude},${p2.longitude}&output=json&callback=cb&key=WSKBZ-B42K4-ZZJUN-DBAOS-GOV4H-63FD4`,
                            success(res) {
                                const data: any = res.data
                                if (data.status!=0){
                                    return wx.showTip(data.message)
                                }
                                let coors = data.result.routes[0].polyline
                                for (var i = 2; i < coors.length; i++) {
                                    coors[i] = coors[i - 2] + coors[i] / 1000000
                                }
                                let points: any = []
                                for (var i = 0; i < coors.length; i += 4) {
                                    points.push({
                                        latitude: coors[i],
                                        longitude: coors[i + 1]
                                    })
                                }
                                self.polyline.push({
                                    points,
                                    color: "#fa3898",
                                    width: 6,
                                    dottedLine: false,
                                    arrowLine: true,
                                })
                                self.map.includePoints({
                                    padding: [20, 20, 20, 20],
                                    points: [p1, p2]
                                })
                            },
                            fail() {
                                wx.showTip(`规划路径失败,请重试`)
                            }
                        })
                    },
                    fail() {
                        wx.showTip(`获取位置失败`)
                    }
                })
            }
        })
    }
    beforeMount() {
        this.map = wx.createMapContext('map')
    }
}
