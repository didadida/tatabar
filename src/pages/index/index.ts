import { Vue, Component } from 'vue-property-decorator'
import { acquireUsers, bannerList } from '$api'

@Component
export default class Index extends Vue {
    banners: BannerList = []
    imgUrls = []
    catchtap(e) {
        const detail = e.mp.target.id
        if (detail) {
            //todo 
        }
    }
    getUsers() {
        this.loading = true
        acquireUsers({ clubUniq: this.$store.state.clubUniq, pageNum: this.pageNo })
            .then(rs => {
                this.pages = rs
                this.rows = (this.pageNo == 1 ? [] : this.rows).concat(rs.rows)
            }).finally(() => this.loading = false)
        console.error('tianshi',Promise.prototype.finally)
        console.error(acquireUsers({ clubUniq: this.$store.state.clubUniq, pageNum: this.pageNo }))
    }
    async getBanner() {
        this.banners = await bannerList()
    }
    onPullDownRefresh() {
        wx.stopPullDownRefresh()
        this.pageNo = 1
        this.getUsers()
    }
    onReachBottom() {
        if (!this.haveMore || this.loading) return;
        this.pageNo++
        this.getUsers()
    }
    mounted() {
        this.getUsers()
        this.getBanner()
    }
}
