import { Vue, Component } from 'vue-property-decorator'
import { userDetails, follow } from '$api'
@Component
export default class Index extends Vue {
    tabs = ["相册", "Ta的礼物"]
    activeIndex = 0
    current = 1
    x = 0
    y = 0
    userId: string
    userInfo: UserDetail = {
        combos: [],
        albums: []
    }
    get bigImages(): string[] {
        return this.userInfo.albums.map(v => v.imageUrl)
    }

    get posx(): string {
        return `translate3d(${(1 - this.current) * 33}vw,0,0)`
    }
    get link(): string {
        const info = this.userInfo
        return `/pages/message/main?${encodeURIComponent(`name=${info.nickName}&sig=${info.usersig}&sex=${info.sex}&thumbnail=${info.thumbnail}&userImage=${info.userImage}`)}`
    }
    //滑动临界值
    get critical(): { left: number, right: number } {
        const m = this.userInfo.albums.filter(v => v)
        return {
            left: m.length <= 2 ? 1 : 0,
            right: m.length <= 2 ? m.length : m.length - 1
        }
    }

    gifts: { giftName: string, giftUrl: string, count: number }[] = []

    tabClick(e) {
        this.activeIndex = e.currentTarget.id;
    }
    chatWith() {
        if (this.loading) return;
        if (!this.$store.state.logined) return wx.showTip(`正在登陆聊天系统，请稍等`);
        let body: MsgItem = {
            identifier: this.userInfo.identifier!,
            lastMsg: '',
            sex: this.userInfo.sex!,
            time: '刚刚',
            identifierNick: this.userInfo.nickName!,
            thumbnail: this.userInfo.thumbnail!,
            unread: 0,
            isMe: 0
        }
        this.$store.commit('toggleChatWith', body)
        setTimeout(() => {
            wx.navigateTo({ url: '/pages/message/chat/main' })
        }, 100);
    }
    touchstart(e) {
        this.x = e.pageX
        this.y = e.pageY
    }
    touchend(e) {
        const x = e.mp.changedTouches[0].pageX - this.x
        const y = e.mp.changedTouches[0].pageY - this.y
        if (Math.abs(x) <= Math.abs(y) || Math.abs(x) <= 20) {
            return
        }
        if (x > 0 && this.current != this.critical.left) {
            this.current--
        } else if (x < 0 && this.current != this.critical.right) {
            this.current++
        }
    }
    preview(index) {
        if (!index) {
            return wx.previewImage({
                current: this.userInfo.userImage,
                urls: [this.userInfo.userImage!]
            })
        }
        if (this.current != index) return;
        wx.previewImage({
            current: this.userInfo.albums[index].imageUrl,
            urls: this.bigImages
        })
    }
    watch(status) {
        if (status == 1) {
            this.watchNext(1)
        } else {
            const self = this
            wx.showModal({
                title: '',
                content: '确定要取消关注吗',
                success(res) {
                    if (res.confirm) {
                        self.watchNext(2)
                    }
                }
            })
        }
    }
    async watchNext(type) {
        const rs = await follow({
            type,
            followUserId: this.userInfo.userId!
        })
        this.userInfo.foStatus = rs.status
        this.userInfo.fansDto = Number(this.userInfo.fansDto) + (type == 1 ? 1 : -1)
    }
    async beforeMount() {
        wx.setTitle(this.$root.$mp.query.userName)
        this.userInfo.sex = this.$root.$mp.query.sex
        this.userInfo.nickName = this.$root.$mp.query.userName
        this.userInfo = await userDetails({ userId: this.$root.$mp.query.userId })
        this.loading = false
    }
}

