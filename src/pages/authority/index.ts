import { Vue, Component } from 'vue-property-decorator'
@Component
export default class Index extends Vue {
    info:BarInfo={
        goodsList:[]
    }

    linkTo(){
        const {name,address,lat,lng} = this.info
        wx.navigateTo({
            url: `/pages/map/main?name=${name}&address=${address}&lat=${lat}&lng=${lng}`
        })
    }
    
    call(){
        wx.makePhoneCall({
            phoneNumber: this.info.phone as any,
        })
    }
    beforeMount() {
        
    }
}