import { Vue, Component } from 'vue-property-decorator'
@Component
export default class Index extends Vue {
    getUserInfo(info) {
        if (info.mp.detail.userInfo){
            setTimeout(() => {
                this.$store.commit("login", info.mp.detail.userInfo)
            }, 1000);
        }else{
            console.log("拒绝授权")
            wx.navigateBack({
                delta: 0
            })
        }
    }
}