import { upload } from '@/http/config'
import { Vue } from 'vue-property-decorator'
const maxSide = 600
wx.showTip = (msg: string = '', duration=2000) => wx.showToast({
    title: msg,
    icon:'none',
    duration
})
wx.setTitle = (title: string) => wx.setNavigationBarTitle({ title })
wx.modal = (msg: string): Promise<any> => new Promise(resolve => {
    wx.showModal({
        title: '',
        content: msg,
        success(rs) {
            if (rs.confirm) {
                resolve(null)
            }
        }
    })
})
wx.loading = (title: string = '') => wx.showLoading({ title })
wx.rerender = (arg: TPage<any>): void => { arg.rows = [...arg.rows] }
wx.getImageInfo2 = (imgPath) => {
    return new Promise((resolve, reject) => {
        wx.getImageInfo({
            src: imgPath,
            success(rs: wx.ImageInfo) {
                let width = rs.width, height = rs.height, scale = width / height
                if (width > maxSide || height > maxSide) {
                    if (width > height) {
                        console.log("横图")
                        width = maxSide
                        height = Math.floor(maxSide / scale)
                    } else {
                        height = maxSide
                        width = Math.floor(maxSide * scale)
                    }
                }
                resolve({ width, height })
            },
            fail(err) {
                reject(err)
            }
        })
    })
}
wx.getFileSize = (imgPath) => {
    return new Promise((resolve, reject) => {
        wx.getFileInfo({
            filePath: imgPath,
            success(rs) {
                resolve(rs.size)
            },
            fail(err) {
                reject(err)
            }
        })
    })
}
wx.compress = (canvasid, imgPath, width, height) => {
    return new Promise((resolve, reject) => {
        const ctx = wx.createCanvasContext(canvasid)
        ctx.drawImage(imgPath, 0, 0, width, height)
        ctx.draw()
        setTimeout(() => {
            wx.canvasToTempFilePath({
                canvasId: canvasid,
                x: 0,
                y: 0,
                width,
                height,
                success(res) {
                    console.log("压缩完成", res)
                    resolve(res.tempFilePath)
                },
                fail(err) {
                    console.error(err)
                    reject(err)
                }
            })
        }, 2000)
    })
}
//type:1.上传相册2.上传头像
wx.uploadFile2 = (arg) => {
    return new Promise((resolve, reject) => {
        wx.getFileInfo({
            filePath: arg.filePath,
            success(rs) {
                if (rs.size > 500 * 1024) {
                    //限制500KB大小
                    wx.getImageInfo({
                        //获取图片宽高。计算压缩宽高
                        src: arg.filePath,
                        success(rs: wx.ImageInfo) {
                            let width = rs.width, height = rs.height, scale = width / height
                            if (width > maxSide || height > maxSide) {
                                if (width > height) {
                                    width = maxSide
                                    height = Math.floor(maxSide / scale)
                                } else {
                                    height = maxSide
                                    width = Math.floor(maxSide * scale)
                                }
                            }
                            Vue.prototype.$store.commit('adjustCanvas', { width, height })
                            //利用canvas压缩
                            const ctx = wx.createCanvasContext(arg.canvasId)
                            ctx.drawImage(arg.filePath, 0, 0, width, height)
                            ctx.draw(false, setTimeout(() => {
                                //canvas导出为微信虚拟图片路径
                                wx.canvasToTempFilePath({
                                    canvasId: arg.canvasId,
                                    x: 0,
                                    y: 0,
                                    width,
                                    height,
                                    success(res) {
                                        console.log("压缩完成，地址为：", res.tempFilePath)
                                        upload(res.tempFilePath, arg.type)
                                            .then(rs => resolve(rs))
                                            .catch(err => reject(err))
                                    },
                                    fail(err) {
                                        reject(err)
                                    }
                                })
                            }, 100))
                            console.log(ctx)

                        },
                        fail(err) {
                            reject(err)
                        }
                    })
                } else {
                    upload(arg.filePath, arg.type)
                        .then(rs => {
                            console.log(11111111)
                            resolve(rs)
                        })
                        .catch(err => {
                            console.log(2222222)
                            reject(err)
                        })
                }
            },
            fail(err) {
                reject(err)
            }
        })
    })
}