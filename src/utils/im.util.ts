const minute = 60, hour = 60 * minute, day = 24 * hour, month = 30 * day, year = month * 12
function getContent(isText: boolean, el: CElem) {
    if (isText) {
        return (el.getContent() as CText).getText()
    }
    const cnt = el.getContent() as CCustom
    return {
        giftName: cnt.getData(),
        giftUrl: cnt.getDesc(),
        giftCount: cnt.getExt()
    }
}

export function pageToBottom(id: string) {
    setTimeout(() => {
        wx.createSelectorQuery().select(id).boundingClientRect((rect: wx.NodesRefRect) => {
            wx.pageScrollTo({
                scrollTop: rect.height
            })
        }).exec()
    }, 100);
}

export function filterMsg(item: Msg,loading?:boolean): chatInfo {
    const el = item.getElems()[0]
    const isText = el.getType() === wx.im.MSG_ELEMENT_TYPE.TEXT   //文本消息
    return {
        loading: !!loading,
        failed: false,
        timeStamp: item.getTime(),
        isText: isText,
        isMe: item.getIsSend(),
        content: getContent(isText, el)
    }
}

export function filterMsgList(msgList: Msg[]): chatInfo[] {
    let list: chatInfo[] = []
    for (let i = 0, len = msgList.length; i < len; i++) {
        const item = msgList[i];
        list.push(filterMsg(item))
    }
    return list
}

export default {
    createMsg(me, msg): MsgItem {
        const isMe = Number(me.userToken == msg.LastC2cMsgFrom_Account)
        return {
            lastMsg: msg.MsgShow === '[其他]' ? (isMe ? '[送出礼物]' : '[收到礼物]') : msg.MsgShow,
            time: this.filterTime(msg.MsgTimeStamp),
            identifier: msg.LastC2cMsgFrom_Account,
            identifierNick: msg.C2cNick,
            unread: 0,
            sex: 0,
            thumbnail: msg.C2cImage,
            isMe
        }
    },

    filterTime(seconds): string {
        const time = Math.floor(Date.now() / 1000 - seconds)
        switch (true) {
            case time < minute:
                return `刚刚`
            case time >= minute && time < hour:
                return `${Math.floor(time / minute)}分钟前`
            case time >= hour && time < day:
                return `${Math.floor(time / hour)}小时前`
            case time >= day && time < month:
                return `${Math.floor(time / day)}天前`
            case time >= month && time < year:
                return `${Math.floor(time / month)}月前`
            case time >= year:
                return `${Math.floor(time / year)}年前`
        }
        return ""
    },
    getProfile(webim, ids) {
        //更新用户资料
        return new Promise(resolve => {
            webim.getProfilePortrait({
                "To_Account": ids,
                "TagList": [
                    "Tag_Profile_IM_Nick",
                    "Tag_Profile_IM_Gender",
                    "Tag_Profile_IM_Image",
                ]
            }, ok => {
                let resultList: any = []
                for (let i = 0, l = ok.UserProfileItem.length; i < l; i++) {
                    let item: any = {}
                    const profileItem = ok.UserProfileItem[i].ProfileItem
                    if (!ok.UserProfileItem[i].ProfileItem) continue;
                    for (let j = 0, len = profileItem.length; j < len; j++) {
                        const user = profileItem[j]
                        switch (user.Tag) {
                            case "Tag_Profile_IM_Nick":
                                item.nickName = user.Value
                                break
                            case "Tag_Profile_IM_Gender":
                                item.sex = user.Value == "Gender_Type_Male" ? 1 : 2
                                break
                            case "Tag_Profile_IM_Image":
                                item.headUrl = user.Value
                                break
                        }
                    }
                    resultList.push(item)
                }
                resolve(resultList)
            }, err => {
                this.getProfile(webim, ids)
            })
        })
    },
}