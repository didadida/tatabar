const im: Webim = require('./webim_wx').default
const util: imUtil = require('./im.util').default

export default function (store) {
    function onConnNotify(resp) {
        var info;
        switch (resp.ErrorCode) {
            case im.CONNECTION_STATUS.ON:
                return
            case im.CONNECTION_STATUS.OFF:
                return wx.showTip('连接已断开，无法收到新消息，请检查下您的网络是否正常')
            case im.CONNECTION_STATUS.RECONNECT:
                return wx.showTip('连接状态恢复正常')
            default:
                wx.showTip('未知连接状态')
        }
    }
    function onMsgNotify(newMsgList: Msg[]) {
        //收到消息推送 . 三种情况
        //1.是当前聊天对象，2，非当前聊天对象但在聊天列表中，3，非当前聊天对象且不在列表中(创建一条)。
        newMsgList.forEach((msg, i) => {
            console.error("收到消息", msg)
            const account = msg.getFromAccount()
            const finded = store.state.msgList.findIndex(v => v.identifier == account)
            const el: CElem = msg.getElems()[0]
            const lastMsg = el.getType() === im.MSG_ELEMENT_TYPE.CUSTOM ? '[收到礼物]' : (el.getContent() as CText).getText()
            if (finded != -1) {
                //第一第二种情况
                const item = store.state.msgList[finded]
                item.lastMsg = lastMsg
                item.time = util.filterTime(Date.now())
                if (account != store.state.currentChatWith.identifier) {
                    item.unread += 1
                } else {
                    wx.onMessage && wx.onMessage(msg)
                }
                store.commit('schedulerMsgList', {
                    type: 'update',
                    index: finded,
                    data: item
                })
            } else {
                //第三种情况
                const newMsg: MsgItem = {
                    lastMsg: lastMsg,
                    time: util.filterTime(msg.getTime()),
                    identifier: account,
                    identifierNick: msg.getFromAccountNick(),
                    unread: 1,
                    sex: 0,
                    thumbnail: '',
                    isMe: 0
                }
                store.commit('schedulerMsgList', {type:'add', data: newMsg })
                util.getProfile(im, [newMsg.identifier]).then(ok => {
                    //更新列表所有信息
                    newMsg.thumbnail = ok[0].headUrl
                    newMsg.identifierNick = ok[0].nickName
                    newMsg.sex = ok[0].sex
                    const index = store.msgList.findIndex(v=>v.identifier==newMsg.identifier)
                    store.commit('schedulerMsgList', {type:'update',index,data:newMsg})
                })
            }
        })
    }

    const loginInfo = {
        'sdkAppID': 1400213918, //用户所属应用id,必填
        'accountType': 1,
        'identifier': store.state.userInfo.userToken, //当前用户ID,必须是否字符串类型，选填
        'identifierNick': store.state.userInfo.nickName, //当前用户昵称，选填
        'userSig': store.state.userInfo.usersig, //当前用户身份凭证，必须是字符串类型，选填
    }
    const listeners = {
        onConnNotify: onConnNotify, //监听连接状态回调变化事件,必填
        onMsgNotify: onMsgNotify
    }

    im.login(loginInfo, listeners, null as any, () => {
        store.state.logined = true
        im.getRecentContactList({ Count: 500 }, ok => {
            let curMsgList: MsgItem[] = store.state.msgList
            let msgList: MsgItem[] = (ok.SessionItem || []).map(v => util.createMsg(store.state.userInfo, v))
            for (let index = 0, len = msgList.length; index < len; index++) {
                const element = msgList[index];
                const item = curMsgList.find(v => v.identifier == element.identifier)
                //如果原来的聊天列表中已经存在此人
                if (item) {
                    element.sex = item.sex
                    element.unread = item.unread
                }
            }
            store.commit('schedulerMsgList', { data: msgList })
            util.getProfile(im, msgList.map((v, index) => v.identifier)).then(ok => {
                //更新列表所有信息
                msgList.forEach((item, index) => {
                    if (!ok[index]) return
                    item.thumbnail = ok[index].headUrl
                    item.identifierNick = ok[index].nickName
                    item.sex = ok[index].sex
                    store.commit('schedulerMsgList', { data: msgList })
                });
            })
            im.syncMsgs(rs => {
                //获取未读消息
                const map = im.MsgStore.sessMap()
                msgList.forEach(v => {
                    for (let key in map) {
                        const k = key.replace("C2C", "")
                        if (v.identifier == k) {
                            v.unread = map[key].unread()
                            return
                        }
                    }
                    v.unread = 0
                })
                store.commit('schedulerMsgList', { data: msgList })
            })
        })
    }, err => {
        console.error(err)
        wx.showTip("聊天系统登录失败,请退出重试")
    })
    return im
} 
