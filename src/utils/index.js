function formatNumber (n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

export function formatTime (date,format="") {
  const split = format.includes("/")?"/":"-"
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join(split)
  const t2 = [hour, minute, second].map(formatNumber).join(':')
  const isGetDate = format.includes("/")||format.includes("-")
  const isGetTime = format.includes(":")
  const len = format.length
  if(isGetDate&&isGetTime){
    return t1+" "+t2
  }else if(isGetDate){
    return t1.substring(0,len)
  }else if(isGetTime){
    return t2.substring(0, len)
  }else{
    return t1 + " " + t2
  }
}

export default {
  formatNumber,
  formatTime
}
