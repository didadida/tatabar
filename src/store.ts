import Vue from 'vue'
import Vuex from 'vuex'
import { login } from '@/http/config'

Vue.use(Vuex)
let rejectCallback, resolveCallback
const msgList: MsgItem[] = wx.getStorageSync("msgList") || []
const store = new Vuex.Store({
  state: {
    clubUniq: "db9177c56d754effae5fa5183fb68c8d",
    canwidth: 0,
    canheight: 0,
    userInfo: JSON.parse(wx.getStorageSync("userInfo") || JSON.stringify({
      nickName: '',
      sex: 0,
      avatarUrl: '',
      thumbnail: '',
      userImage: '',
      imageUrl: '',
      birthday: '',
      phone: '',
      hobby: '',
      customSign: '',
      combos: [],//礼物
      amount: 0,
      usersAlbums: [],
      usersig: ''
    })),
    msgList: JSON.parse(wx.getStorageSync("msgList") || '[]'),
    currentChatWith: {},
    logined: false,
    gift: null
  },
  actions: {
    login({ commit }) {
      return new Promise((resolve, reject) => {
        resolveCallback = resolve
        rejectCallback = reject
        wx.getUserInfo({
          success(rs) {
            commit('login', rs.userInfo)
          },
          fail() {
            wx.redirectTo({
              url: "/pages/login/main"
            })
          }
        })
      })
    }
  },
  mutations: {
    toggleChatWith(state, chatWith: MsgItem) {
      const item = state.msgList.find(v => v.identifier == chatWith.identifier)
      if (!item) {
        state.msgList.unshift(chatWith)
        state.currentChatWith = chatWith as any
      } else {
        state.currentChatWith = item
      }
      wx.setStorageSync('msgList', JSON.stringify(state.msgList))
    },

    schedulerMsgList(state, action: { type?: 'add' | 'delete' | 'update' | 'create', index?: number, data?: any }) {
      switch (action.type) {
        case 'add':
          state.msgList.unshift(action.data)
          break
        case 'delete':
          state.msgList.splice(action.index, 1)
          break
        case 'update':
          Object.assign(state.msgList[action.index!], action.data)
          break
        default:
          state.msgList = action.data
      }
      wx.setStorageSync('msgList', JSON.stringify(state.msgList))
      let count = state.msgList.reduce((sum, v) => sum + v.unread, 0)
      if (count > 0) {
        wx.setTabBarBadge({
          "index": 2,
          "text": count > 0 ? count.toString() : ''
        })
      } else {
        wx.removeTabBarBadge({ "index": 2 })
      }
    },
    //更新userinfo
    updateUserInfo(state, info) {
      Object.assign(state.userInfo, info)
      wx.setStorageSync("userInfo", JSON.stringify(state.userInfo))
    },
    //调整压缩canvas大小
    adjustCanvas(state, arg) {
      state.canwidth = arg.width
      state.canheight = arg.height
    },
    //全局登录
    login(state, userInfo: wx.UserInfo) {
      wx.redirectTo({
        url: "/pages/index/main"
      })
      var self = this
      wx.login({
        success(rs) {
          login({
            nickName: userInfo.nickName,
            avatarUrl: userInfo.avatarUrl,
            sex: userInfo.gender,
            tableCode: "",//酒吧编号
            code: rs.code,
            clubUniq: state.clubUniq
          }).then(rs => {
            if (rs.data.code == 200) {
              resolveCallback(rs)
            } else {
              rejectCallback(rs)
            }
          }).catch(err => {
            rejectCallback(err)
          })
        },
        fail(err) {
          rejectCallback(err)
        }
      })
    }
  }
})

export default store
console.log("store加载完成...")