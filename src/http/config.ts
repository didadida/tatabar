import FlyIo from 'flyio/dist/npm/wx'
import { Fly, FlyRequestConfig } from 'flyio/index'
import { Vue } from 'vue-property-decorator'

const fly: Fly = new FlyIo()
const newFly: Fly = new FlyIo()
const retry = 2 //重新请求次数.注意是重新请求
const sesstionKey = "cookie"
let scheduler: FlyRequestConfig[] = []
//实列配置
fly.config.baseURL = 'http://qa.lailaiche.com/tata/mini'
// fly.config.baseURL = 'https://tata.fishsaas.com/tata/mini'
//15秒超时
fly.config.timeout = 15000
fly.config.headers['content-type'] = 'application/x-www-form-urlencoded'
fly.config.headers[sesstionKey] = wx.getStorageSync(sesstionKey)
//登录请求实例不设置超时.newFly 专用于登录
newFly.config.timeout = 0
newFly.config.baseURL = fly.config.baseURL.replace("mini", "mnlogin")
newFly.config.headers['content-type'] = fly.config.headers['content-type']
fly.interceptors.request.use(req => {
    wx.showNavigationBarLoading()
    //请求添加到队列
    scheduler.push(req)
    //初始化重试次数
    req.retryed = req.retryed || 0
    //如果sessionkey不存在
    if (!req.headers[sesstionKey]) {
        //未登录，全局实例异步锁
        fly.lock()
        return Vue.prototype.$store.dispatch('login').then(rs => {
            const cookie = fly.config.headers[sesstionKey] = req.headers[sesstionKey] = rs.headers['set-cookie'][0]
            wx.setStorageSync(sesstionKey, cookie)
            Vue.prototype.$store.commit('updateUserInfo', rs.data.data)
            return req
        }).catch(err => {
            console.error(err)
            wx.showTip(`连接失败，请检查网络或退出重试`)
        }).finally(() => {
            fly.unlock()
        })
    }
    for (let key in (req.body || {})) {
        if (!req.body[key] && req.body[key] != 0) {
            delete req.body[key]
        }
    }
    return req
})

fly.interceptors.response.use(ok => {
    scheduler.splice(scheduler.findIndex(v => ok.request.url == v.url), 1)
    if (!scheduler.length) {
        wx.hideNavigationBarLoading()
    }
    if (ok.request.abort) {
        return Promise.reject(`abort`)
    }
    const data = ok.data
    if (data.code == -1) {
        //登录失效，清空sesstonKey以确保重新执行后login触发
        fly.config.headers[sesstionKey] = ok.request.headers[sesstionKey] = null
        return fly.request(ok.request.url!, ok.request.body, ok.request)
    }
    if (data.code == 200) {
        return Promise.resolve(data.data)
    }
    if (data.msg) {
        wx.showTip(data.msg)
    }
    return Promise.reject(data)
}, (err: any) => {
    console.log(err)
    if (err.status == 1 && err.request.retryed < retry) {
        err.request.retryed++
        console.log('第' + err.request.retryed + '次重新请求')
        return fly.request(err.request.url, err.request.body, err.request)
    }
    if (err.status == 1) {
        wx.showTip(`请求超时，请稍后再试`)
    } else {
        wx.showTip(`网络错误，请检查网络或稍后再试`)
    }
    return Promise.reject(err.status)
})

export function base(method, url, data):Promise<any> {
    return new Promise((resovle,reject)=>{
        if (data === "abort") {
            //true表示打断
            const item = scheduler.find(v => v.url === url)
            if (item) { 
                item.abort = true
                return
            }
        }
        //检查当前请求是否存在
        const item = scheduler.find(v => v.url === url)
        //如果存在且未被打断。那么忽略该次请求。
        if (item && !item.abort) return
        fly[method](url, data).then(rs=>resovle(rs)).catch(err=>reject(err))
        // return resovle(fly[method](url, data))
    })
}
export const get = (url, data?) => base("get", url, data)
export const post = (url, data?) => base("post", url + '/', data)
export function upload(filePath: string, type: string): Promise<any> {
    return new Promise((resolve, reject) => {
        wx.uploadFile({
            url: fly.config.baseURL + '/uploadUsersAlbum', // 请求服务器地址
            filePath,
            formData: { 'type': String(type) },  // 请求的参数
            name: 'file',
            header: {  // 设置请求的 header，header 中不能设置 Referer。
                'content-type': 'multipart/form-data', // 默认值
                'cookie': wx.getStorageSync(sesstionKey)
            },
            success(res) { // 收到开发者服务成功返回的回调函数
                // success
                const data = JSON.parse(res.data)
                if (data.code != 200) {
                    console.log('wx upload rs !=200')
                    return reject(data.msg)
                }
                resolve({
                    id: data.data.id,
                    imageUrl: filePath,
                    thumbnail: filePath,
                });
            },
            fail(err) { // 接口调用失败的回调函数
                console.log('wx upload rj')
                reject(err)
            }
        })
    })
}
export const login = (data?, callback?) => newFly.post("/login", data)
export const abort = point => point("abort")
export const baseUrl = fly.config.baseURL
export default fly


