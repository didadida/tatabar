import { get, post } from './config'
import { FlyPromise } from 'flyio';
// 通用说明：
// 登录成功后会返回sessionid和usertoken
// sessionid为登录凭证, userToken为用户唯一标识
// 请求每个接口需要带上sessonid，接口返回code有三种情况:
// 0 接口错误信息
//     - 1, "凭证失效，请重新授权" （若返回 - 1，需重新登录）
// 200 接口成功返回数据

//1.礼物套餐展示列表
export const giftList = (data?) => get("/giftList", data)
//2.粉丝、关注列表
export const followList = (data: { type: number, pageNo: number }): Promise<FansAndFollow> => get("/followList", data)
//3.订单列表 
export const ordersList = (data: {
    statusType: 0 | 1 | 2
    pageNo: number
}): Promise<OrderList> => get("/ordersList", data)
//4.金币明细列表
export const coinTransList = (data?: {
    productId?: number
    pageNo: number
}): Promise<CoinDetail> => get("/coinTransList", data)
//5.个人信息编辑
export const userEdit = (data?) => post("/userEdit", data)
// 6.关注 / 取消关注
export const follow = (data: { type: number, followUserId: number }): Promise<User> => get("/follow", data)
// 7.上传图片(个人照片)
export const uploadUsersAlbum = (data?) => get("/uploadUsersAlbum", data)
// 8.删除相册图片(个人照片)
export const delUsersAlbum = (data?) => post("/delUsersAlbum", data)
// 9.相册列表
export const usersAlbumList = (data?) => get("/usersAlbumList", data)
// 10.产品列表
export const productList = (data?) => get("/productList", data)
// 11.金币明细页面
export const coinTransInfo = (data?): Promise<coinTrans> => get("/coinTransInfo", data)
// 12.金币提现记录列表
export const coinWithdrawalList = (data?): Promise<TPage<any>> => get("/coinWithdrawalList", data)
// 13.获取酒吧用户列表
export const acquireUsers = (data: {
    clubUniq: string | number
    pageNum?: number
    pageSize?: number
    sex?: number
}): Promise<BarUsers> => get("/acquireUsers", data)
// 14.推荐商品列表
export const wxGoodsList = (data?) => get("/wxGoodsList", data)
// 15.修改相册照片顺序
export const updateUsersAlbum = (data?) => post("/updateUsersAlbum", data)
// 16.取消订单 
export const wxcancelOrder = (data: { orderId: string }): Promise<DrinkOrder["rows"][0]> => get("/wxcancelOrder", data)
// 17.提现
export const toWeixinWithdrawal = (data?) => get("/toWeixinWithdrawal", data)
// 18.获取个人中心数据
export const centerData = (): Promise<MineDetal> => get("/centerData")
// 19.获取用户详情数据
export const userDetails = (data: { userId: number }): Promise<UserDetail> => get("/userDetails", data)
// 20.获取资料修改页面信息
export const editInfo = (data?) => get("/editInfo", data)
// 21.获取酒吧详情资料
export const clubDetails = (data?): any => get("/clubDetails", data)
// 22.获取提现页面信息
export const withdrawalInfo = (data?) => get("/withdrawalInfo", data)
// 23.获取排行榜信息
export const giftRankDetail = (data: {
    clubUniq: number,
    rankType: number,
}): Promise<GiftRank> => get("/giftRankDetail", data)
// 24.根据userToken返回IM信息
export const acquireIMSet = (data?) => get("/acquireIMSet", data)
// 25.获取酒吧banner列表
export const bannerList = (): Promise<BannerList> => get("/bannerList")
// 26.获取酒水订单
export const goodsOrdersList = (data: { type: 0 | 1, pageNo: number, pageSize: number }): Promise<DrinkOrder> => get("/goodsOrdersList", data)
//30.确认支付(针对待支付订单)
export const confirmToPay = (data: {
    orderId: number,
    type: 0 | 1
}): Promise<PayOrder> => get("/confirmToPay", data)

// 27.获取临时凭证
export const mnGetLoginSession = (): Promise<any> => get("/mnGetLoginSession")
// 27.实物商品列表
export const goodsList = (data?): Promise<any> => get("/goodsList",data)
export const toWeixinPay = (data?): Promise<any> => get("/toWeixinPay",data)

export const ssr = (...args: any[]): Promise<any[]> => {
    let results = new Array(args.length).fill(false)
    return new Promise((resolve, reject) => {
        args.forEach((v, i) => {
            v.then(rs => {
                results[i] = rs
                if (results.findIndex(v => v === false) == -1) {
                    resolve(results)
                }
            }).catch(err => {
                reject(err)
            });
        })
    })
}